Sidekiq.configure_server do |config|
  config.average_scheduled_poll_interval = 2
  config.redis = {
    url: ENV["REDIS_URL"],
    port: ENV["REDIS_PORT"],
    password: ENV["REDIS_PASSWORD"],
    namespace: 'queue'
  }
end

Sidekiq.configure_client do |config|
  config.redis = {
    url: ENV["REDIS_URL"],
    port: ENV["REDIS_PORT"],
    password: ENV["REDIS_PASSWORD"],
    namespace: 'queue'
  }
end