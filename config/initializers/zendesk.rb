require 'zendesk_api'

$zendesk = ZendeskAPI::Client.new do |config|
  config.url = "https://agentecho.zendesk.com/api/v2"
  config.username = "ken@agentecho.io"
  config.token = ENV['ZENDESK_API_KEY']

  config.retry = true

  require 'logger'
  config.logger = Logger.new(STDOUT)
end
