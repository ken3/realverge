Rails.application.config.session_store(
  :cookie_store,
  key: '_agentecho_session',
  expire_after: 10.years
)