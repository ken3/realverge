module OmniAuth::Strategies

  class FacebookIntegration < Facebook
    def name
      :facebook_integration
    end
  end

  class GoogleIntegration < GoogleOauth2
  end

end

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :google_oauth2, ENV["GOOGLE_CLIENT_ID"], ENV["GOOGLE_CLIENT_SECRET"],
    {
      name: "google",
      scope: "email,profile",
      prompt: "select_account",
      image_aspect_ratio: "square",
      image_size: 50,
      path_prefix: '/auth',
      provider_ignores_state: Rails.env.development?
    }

  provider :facebook, ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_APP_SECRET'],
    {
      scope: 'email,public_profile',
      display: 'page',
      path_prefix: '/auth',
      provider_ignores_state: Rails.env.development?
    }

  provider OmniAuth::Strategies::GoogleIntegration, ENV["GOOGLE_CLIENT_ID_INTEGRATION"], ENV["GOOGLE_CLIENT_SECRET_INTEGRATION"],
    {
      name: "google_integration",
      scope: "email,profile,https://www.googleapis.com/auth/spreadsheets.readonly,https://www.googleapis.com/auth/drive.readonly",
      prompt: "select_account",
      image_aspect_ratio: "square",
      image_size: 50,
      path_prefix: '/integrations',
      provider_ignores_state: Rails.env.development?
    }

  provider :facebook_integration, ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_APP_SECRET'],
    {
      scope: 'public_profile,pages_show_list,read_insights',
      display: 'page',
      path_prefix: '/integrations',
      provider_ignores_state: Rails.env.development?
    }
end