require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module Workspace
  class Application < Rails::Application
    config.react.addons = true

    config.eager_load_paths += ["#{config.root}/app/models/integrations"]

    config.cache_store = :redis_store, {
      url: ENV["REDIS_URL"],
      port: ENV["REDIS_PORT"],
      password: ENV["REDIS_PASSWORD"],
      namespace: 'cache',
      maxmemory: '40mb',
      'maxmemory-policy': 'allkeys-lru'
    }
    config.default_url_options = { host: ENV["SMTP_DOMAIN"] }
    routes.default_url_options = { host: ENV["SMTP_DOMAIN"] }

    Recaptcha.configure do |config|
      config.public_key  = '6LfqTykTAAAAAFb3NDyWYOUvnMsLdwkE5KypoDa8'
      config.private_key = ENV['RECAPTCHA_API_KEY']
    end
  end
end