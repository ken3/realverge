Rails.application.routes.draw do
  default_url_options host: ENV["DOMAIN"]
  mount ActionCable.server => '/cable'

  # App Routes
  root to: 'dashboards#index'
  resources :dashboards,  except: [:new]
  resources :loops,       except: [:new, :edit]
  resources :connections, only:   [:show, :index, :create, :destroy], as: :integrations, controller: :integrations
  resources :glances,     except: [:new, :edit, :show, :index], as: :graphs, controller: :graphs
  resources :accounts,    except: [:edit]

  # Custom Routes and Actions
  patch '/dashboards/:id/duplicate'       => 'dashboards#duplicate',       as: :duplicate_dashboard
  patch '/dashboards/:id/reset-speakeasy' => 'dashboards#reset_speakeasy', as: :reset_dashboard_speakeasy
  patch '/loops/:id/reset-speakeasy'      => 'loops#reset_speakeasy',      as: :reset_loop_speakeasy
  patch '/glances/:id/reset-speakeasy'    => 'graphs#reset_speakeasy',     as: :reset_graph_speakeasy
  get   '/settings'                       => 'accounts#edit',              as: :settings
  get   '/billing'                        => 'accounts#billing',           as: :billing
  get   '/referral'                       => 'accounts#referral',          as: :referral
  post  '/stripe'                         => 'accounts#stripe'
  put   '/update-billing'                 => 'accounts#update_billing'
  get   '/signup(/:referrer_hash)'        => 'accounts#new',               as: :signup
  get   '/password'                       => 'accounts#password',          as: :password
  get   '/reset-password'                 => 'accounts#reset_password',    as: :reset_password
  post  '/password-auth'                  => 'accounts#password_auth'
  patch '/password-update'                => 'accounts#update_password'
  post  '/accounts'                       => 'accounts#create'
  get   '/login'                          => 'sessions#new',               as: :login
  post  '/login'                          => 'sessions#create'
  get   '/logout'                         => 'sessions#destroy',           as: :logout
  put   '/integrations/:id/autogenerate'  => 'integrations#autogenerate',  as: :autogenerate

  # Omniauth for Login
  get  'auth/:provider'          => 'sessions#create', as: :omniauth
  get  'auth/:provider/callback' => 'sessions#create'

  # Omniauth for Connections
  get  'integrations/:provider'          => 'integrations#create', as: :connection
  get  'integrations/:provider/callback' => 'integrations#create'

  # SpeakeasyLink Support
  get  '/board/:uuid' => 'dashboards#show',  as: :public_board
  get  '/loop/:uuid'  => 'loops#show',       as: :public_loop
  get  '/:uuid'       => 'graphs#edit',      as: :public_graph_form
  put  '/:uuid'       => 'graphs#update'

  # API and Webhook Routes
  namespace :api do
    namespace :zapier do
      resources :glances, only: [:index, :show], as: :graphs, controller: :graphs
      post 'glances/:id' => 'graphs#update'
    end
    namespace :hubspot do
      post :webhook, as: :graphs, controller: :graphs
    end
    namespace :google do
      post :webhook, as: :graphs, controller: :graphs
    end
  end
end