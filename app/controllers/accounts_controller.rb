class AccountsController < ApplicationController
  protect_from_forgery except: :stripe
  before_action :set_account, only: [:edit, :update, :update_billing, :billing]
  before_action :authorize, only: [:show, :edit, :update, :index, :destroy, :update_billing, :billing, :referral]

  def new
    @title    = 'Sign up'
    @source   = params[:s] || ''
    @account  = Account.new()
    @referrer = (params[:referrer_hash] && Base64.urlsafe_decode64(params[:referrer_hash])) || ''
  end

  def edit
    @title = 'Account Settings'
  end

  def billing
    @title = 'Account Billing'
  end

  def referral
    @title = 'Account Referral'
  end

  def update_billing
    current_account.update_columns(
      stripe_token: params[:stripe_token],
      cc_last_four: params[:cc_last_four]
    )
    @account.add_card!(params[:stripe_token])
    @account.update_billing_cycle!(@account.cycle_ends_on)
    redirect_back(fallback_location: settings_path(current_account), notice: 'New credit card added.')
  end

  def password
    redirect_to login_path if (current_account.nil? and params[:token].nil?)
    @account = current_account || Account.find_by(password_reset_token: params[:token])
    if ((@account.reset_password_date || DateTime.now) + 3.days) > DateTime.now.at_midnight
      @title = 'Change Password'
    else
      redirect_to login_path
    end
  end

  def reset_password
    @title = 'Reset Password'
  end

  def create
    @account = Account.new(account_params)
    @account.save ?
      (session[:account_id] = @account.id; redirect_to root_path(n: :t), notice: 'Welcome to AgentEcho!') :
      (render json: @account.errors, status: :unprocessable_entity)
  end

  def update
    @account.update(account_params) ?
      (redirect_back(fallback_location: settings_path(@accoaunt), notice: 'Account was successfully updated.')) :
      (render json: @account.errors, status: :unprocessable_entity)
  end

  def update_password
    redirect_to login_path if (current_account.nil? and params[:token].nil?)
    @account = current_account || Account.find_by(password_reset_token: params[:token])
    if @account.update(password: account_params[:password], password_confirmation: account_params[:password_confirmation])
      current_account.nil? ?
        (redirect_to login_path, notice: 'Password was updated') :
        (redirect_to settings_path(@account), notice: 'Password was updated')
    else
      render json: @account.errors, status: :unprocessable_entity
    end
  end

  def password_auth
    if(account = Account.find_by_email(params[:email]))
      account.reset_password!
      redirect_to login_path, notice: "Link to reset password was sent to #{account.email}"
    else
      render json: { email: ['not found'] }.to_json, status: :unprocessable_entity
    end
  end

  # Support actions
  def index
    @accounts = Account.all.order(:created_at).reverse
    super_account? ?
      (render) :
      (redirect_to logout_path)
  end

  def show
    super_account? ?
      (session[:account_id] = params[:id]; redirect_to root_path) :
      (redirect_to logout_path)
  end

  def destroy
    account = Account.find(params[:id])
    super_account? ?
      (account.destroy; redirect_to accounts_path, notice: "Account: '#{account.email}' was deleted.") :
      (redirect_to logout_path)
  end

  def stripe
    event_json = JSON.parse(request.body.read)
    account = Account.find(stripe_id: event_json['data']['object']['customer'])
    if event_json['type'] == 'charge.succeeded'
      subscription = account.stripe_subscription
      account.update_billing!(subscription.trial_end)
    elsif event_json['type'] == 'charge.failed'
      account.failed_billing!(event_json['data']['object']['failure_message'])
    end
    head :ok
  end

  private

  def super_account?
    [ 'ken@agentecho.io',
      'brian@agentecho.io',
      'rachel@agentecho.io'
    ].include?(current_account.email)
  end

  def set_account
    @account = current_account
  end

  def account_params
    params.require(:account).permit(
      :email,
      :password,
      :password_confirmation,
      :name,
      :phone,
      :time_zone,
      :email_optin,
      :source,
      :persona,
      :referrer_id,
      :plan
    )
  end
end