class DashboardsController < ApplicationController
  before_action :set_dashboard, only: [:show, :edit, :reset_speakeasy, :update, :duplicate, :destroy]
  before_action :authorize, only: [:show], unless: '@dashboard.public_form?'
  before_action :authorize, only: [:index, :new, :create, :edit, :reset_speakeasy, :update, :destroy]

  def index
    @title      = 'Boards'
    @dashboard  = Dashboard.new
    @dashboards = current_account.dashboards.order(:created_at)
    @loop       = Loop.new
    @loops      = current_account.loops.order(:created_at)
  end

  def show
    if params[:uuid].present?
      if current_account.present? or @dashboard.public_form?
        @graphs  = @dashboard.graphs_position_order
        @title   = "#{@dashboard.name} Board"
        @preview = params[:preview]
        @dashboard.account.points! if current_account.nil?
        render layout: 'dashboard'
      else
        redirect_to login_path, notice: 'You must login to access this board.'
      end
    else
      redirect_to public_board_path(@dashboard.uuid, preview: params[:preview])
    end
  end

  def edit
    @title = "Edit #{@dashboard.name} Board"
    @graphs = @dashboard.graphs_position_order
    @graph = Graph.new(dashboard: @dashboard)
  end

  def create
    @dashboard = Dashboard.new(dashboard_params)
    @dashboard.account = current_account
    @dashboard.save ?
      (redirect_to edit_dashboard_path(@dashboard), notice: 'Dashboard was successfully created.') :
      (render json: @dashboard.errors, status: :unprocessable_entity)
  end

  def duplicate
    @dashboard.duplicate!(dashboard_params[:name]) ?
      (redirect_back(fallback_location: dashboards_path, notice: 'Dashboard was successfully duplicated.')) :
      (render json: @dashboard.errors, status: :unprocessable_entity)
  end

  def reset_speakeasy
    @dashboard.disable_open_boards!
    @dashboard.regenerate_uuid!
    @dashboard.touch
    render json: { link: public_board_url(@dashboard.reload.uuid), speakeasy: true }.to_json
  end

  def update
    @dashboard.update(dashboard_params) ?
      (redirect_to edit_dashboard_path(@dashboard), notice: 'Dashboard was successfully updated.') :
      (render json: @dashboard.errors, status: :unprocessable_entity)
  end

  def destroy
    @dashboard.destroy
    redirect_to root_path, notice: 'Dashboard was successfully deleted.'
  end

  private

  def set_dashboard
    if params[:id].present?
      @dashboard = current_account.dashboards.find(params[:id])
    else
      @dashboard = Dashboard.find_by_uuid(params[:uuid])
    end
    render_404 if @dashboard.nil?
    @current_account = @dashboard.account if current_account.nil?
    @integrations = @current_account.integrations.is_enabled
  end

  def dashboard_params
    params.require(:dashboard).permit(:name, :public_form, positions: [])
  end
end
