class GraphsController < ApplicationController
  before_action :set_graph, only: [:edit, :update, :destroy, :reset_speakeasy]
  before_action :authorize, only: [:create, :destroy, :reset_speakeasy]

  def create
    @graph = Graph.new(graph_params)
    # Don't redirect when pre-creating a zapier glance.
    dont_redirect = params[:dont_redirect].present? and params[:dont_redirect]
    if @graph.save
      dont_redirect ?
        (render json: @graph.reload.to_json) :
        (redirect_to edit_dashboard_path(@graph.dashboard), notice: 'Glance was successfully created.')
    else
      render json: @graph.errors, status: :unprocessable_entity
    end
  end

  def edit
    if allowed?
      @public_form = true
    else
      redirect_to login_path, notice: 'You must login to update this glance.'
    end
  end

  def update
    if allowed?
      if current_account.present? or verify_recaptcha(model: @graph)
        @graph.update(graph_params) ?
          (redirect_back(fallback_location: public_graph_form_path(@graph), notice: 'Glance was successfully updated.')) :
          (render json: @graph.errors, status: :unprocessable_entity)
      else
        render json: @graph.errors, status: :unprocessable_entity
      end
    else
      redirect_to login_path, notice: 'You must login to update this glance.'
    end
  end

  def reset_speakeasy
    @graph.regenerate_uuid!
    render json: { link: public_graph_form_url(@graph.reload.uuid), speakeasy: true }.to_json
  end

  def destroy
    dashboard = @graph.dashboard
    @graph.destroy
    redirect_to edit_dashboard_path(dashboard), notice: 'Glance was successfully destroyed.'
  end

  private

  def allowed?
    @graph.public_form? or current_account == @graph.account
  end

  def set_graph
    @graph = Graph.where("id = ? OR uuid = ?", params[:id], params[:uuid]).first
    @current_account = @graph.account if current_account.nil?
  end

  def graph_params
    # Hashify JSON encoded string from hidden field.
    params[:graph][:data] = ActiveSupport::JSON.decode(params[:graph][:data]) if params[:graph][:data].present?
    params.require(:graph).permit(
      :title,
      :size,
      :public_form,
      :kind,
      :dashboard_id,
      :integration_id,
      data: [
        :value, :src, :iso_code, :sheet_name, :range, :sheet_id, :range_labels, :page_id, :output, :zapier_id,
        labels: [], series: [],
        listings: [:address, :note, :status]
      ]
    )
  end
end