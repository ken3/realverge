class SessionsController < ApplicationController
  def new
    @title = 'Login'
  end

  def create
    if ((account = Account.find_by_email(params[:email]) and
         account.authenticate(params[:password])) or
         account = Account.from_omniauth(env["omniauth.auth"], request.env['omniauth.params']))

      if account.errors.any?
        redirect_to signup_path, alert: 'email address is already in use'
      else
        if account.deleted?
          redirect_to login_path, alert: "That account is no longer active!"
        else
          session[:account_id] = account.id
          account.update_last_login_at unless account.new_record?
          redirect_to root_path
        end
      end
    else
      render json: {"login":["invalid email or password"]}, status: :unprocessable_entity
    end
  end

  def destroy
    session[:account_id] = nil
    redirect_to '/login'
  end
end