class IntegrationsController < ApplicationController
  before_action :set_integration, only: [:show, :destroy, :autogenerate]
  before_action :authorize

  def show
    render json: @integration.retrieve(params)
  end

  def index
    @title       = 'Connections'
    @basic       = current_account.basic_providers
    @premium     = current_account.premium_providers
    @integration = current_account.integrations.find(params['new-connection']) if params['new-connection']
  end

  def create
    @integration = Integration.new(provider: params[:provider])
    @integration.extract_auth(env["omniauth.auth"], request.env['omniauth.params'], params)
    @integration.account = current_account
    @integration.save ?
      (redirect_to integrations_path('new-connection': @integration.id), notice: 'Connection was successfully enabled.') :
      (render json: @integration.errors, status: :unprocessable_entity)
  end

  def destroy
    @integration.destroy
    redirect_to integrations_path, notice: 'Connection was successfully disabled.'
  end

  def autogenerate
    klass = "Integrations::#{@integration.provider.split('_').first.capitalize}Helper".constantize
    klass.seed_new_connection(current_account.id, params[:loop] == 'loop')
    if params[:loop] == 'loop'
      redirect_to root_path, notice: 'Autogenerated Boards and Loops'
    else
      redirect_to root_path, notice: 'Autogenerated Boards'
    end
  end

  private

  def set_integration
    @integration = current_account.integrations.find(params[:id])
  end
end