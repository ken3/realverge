class LoopsController < ApplicationController
  before_action :set_loop,  only: [:show, :edit, :reset_speakeasy, :update, :destroy]
  before_action :authorize, only: [:show], unless: '@loop.public_loop?'
  before_action :authorize, only: [:create, :reset_speakeasy, :update, :destroy]

  def show
    if params[:uuid].present?
      if current_account.present? or @loop.public_loop?
        @title   = "#{@loop.name} Loop"
        @preview = params[:preview]
        @loop.account.points! if current_account.nil?
        render layout: 'dashboard'
      else
        redirect_to login_path, notice: 'You must login to access this loop.'
      end
    else
      redirect_to public_loop_path(@loop.uuid, preview: params[:preview])
    end
  end

  def create
    @loop = Loop.new(loop_params)
    @loop.account = current_account
    @loop.save ?
      (redirect_to root_path, notice: 'Loop was successfully created.') :
      (render json: @loop.errors, status: :unprocessable_entity)
  end

  def reset_speakeasy
    @loop.disable_open_boards!
    @loop.regenerate_uuid!
    @loop.touch
    redirect_back(fallback_location: root_path, notice: 'Link has been reset.')
  end

  def update
    @loop.update(loop_params) ?
      (redirect_to root_path, notice: 'Loop was successfully updated.') :
      (render json: @loop.errors, status: :unprocessable_entity)
  end

  def destroy
    @loop.destroy
    redirect_to root_path, notice: 'Loop was successfully deleted.'
  end

  private

  def set_loop
    if params[:id].present?
      @loop = current_account.loops.find(params[:id])
    else
      @loop = Loop.find_by_uuid(params[:uuid])
    end
    render_404 if @loop.nil?
    @current_account = @loop.account if current_account.nil?
    @integrations = @current_account.integrations.is_enabled
  end

  def loop_params
    params.require(:loop).permit(:name, :public_loop, :duration, :transition, dashboard_ids: [])
  end
end
