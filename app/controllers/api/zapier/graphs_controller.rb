# Zapier API supports these graph types:
# - Number
# - Title
# - Paragraph
# - Currency

class Api::Zapier::GraphsController < Api::ApiController
  before_action :set_graph, except: [:index]

  # Test action, enable integration when triggered.
  def index
    i = @current_account.integrations.where(provider: 'zapier_integration').first
    i.update_columns(enabled: true)
    head :ok
  end

  def show
    render json: @graph, serializer: GraphSerializer
  end

  def update
    @graph.update(graph_params) ?
      (head :ok) :
      (render json: @graph.errors, status: :unprocessable_entity)
  end

  private

  def set_graph
    @graph = @current_account.graphs.where("graphs.data->>'zapier_id' = ?", params[:id])
  end

  def graph_params
    params.require(:graph).permit(
      :title,
      data: [:value, :title, labels: [], series: []]
    )
  end
end