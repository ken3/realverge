class Api::ApiController < ActionController::Base
  before_action :authorize_api

  private

  def authorize_api
    token = request.headers['Authorization']
    integration ||= Integration.where("integrations.auth->>'api_token' = '#{token}'").first
    @current_account ||= integration.account unless integration.nil?
    @current_account.points! unless @current_account.nil?
    render json: { error: 'not-found' }, status: :not_found if @current_account.nil?
  end
end