class Api::Google::GraphsController < ActionController::Base

  def webhook
    UpdateGraphsWorker.perform_async(request.env['HTTP_X_GOOG_CHANNEL_ID'])
    head :ok
  end

end