class Api::Hubspot::GraphsController < ActionController::Base

  def webhook
    portals = params['_json']
    portals.each do |portal|
      id = Integration.where("integrations.auth->>'portal_id' = '?'", portal['portalId']).pluck(:id).first
      UpdateGraphsWorker.perform_async(id) unless id.nil?
    end
    head :ok
  end

end