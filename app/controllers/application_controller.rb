class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  after_action :update_last_seen_at, unless: 'current_account.nil?'
  around_action :set_time_zone,      unless: 'current_account.nil?'
  around_action :add_log_tags,       unless: 'current_account.nil?'

  def current_account
    @current_account ||= Account.find(session[:account_id]) if session[:account_id]
  end
  helper_method :current_account

  def render_404
    raise ActionController::RoutingError.new('Not Found')
  end
  helper_method :render_404

  private

  def authorize
    redirect_to '/login' unless current_account
  end

  def update_last_seen_at
    @current_account.update_column(:last_seen_at, -> { ApplicationHelper::agentecho_time }.call)
    @current_account.points!
  end

  def set_time_zone(&block)
    Time.use_zone(current_account.time_zone, &block)
  end

  def add_log_tags(&block)
    logger.tagged("AccountID: #{current_account.id}", current_account.name, &block)
  end
end
