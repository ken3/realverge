module ViewHelper
  def statusBadgeClass(status)
    case status
    when 'New', 'Active'
      'blue'
    when 'Closed'
      'green'
    when 'Cancelled', 'Withdrawn'
      'red'
    else
      'purple'
    end
  end
end