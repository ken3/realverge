module ApplicationHelper
  APP_VERSION ||= ENV['APP_VERSION']

  def self.agentecho_time
     Time.now.in_time_zone('Pacific Time (US & Canada)')
  end
end
