module AccountHelper
  def seed_new_account(account_id)
    account = Account.find(account_id)

    # Set end of cycle for trial
    account.update_columns(cycle_ends_on: (Time.current + 14.days).to_datetime)

    # Add Zapier Intergration
    IntegrationOperation.generate_zapier!(account)

    # Generating learning board
    dashboard = Dashboard.create(
      account: account,
      name: "Let's Learn 😊"
    )
    dashboard.graphs << Graph.new(size: 3, kind: 'native_number', title: 'This is a Number Glance', data: { output: 'number' })
  end
end