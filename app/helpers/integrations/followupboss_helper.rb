module Integrations::FollowupbossHelper

  def self.seed_new_connection(account_id, include_loop = true)
    account     = Account.find(account_id)
    integration = account.integrations.find_by_provider('followupboss_integration')
    dashboard_ids = []

    Dashboard.transaction do
      fub_calls = Dashboard.create(
        account: account,
        name:    "Follow Up Boss Calls"
      )

      fub_calls.graphs.first.update_columns(size: 6)

      %w(calls-by-day calls-by-week calls-by-month).each do |kind|
        fub_calls.graphs << Graph.new(
          integration: integration,
          size:        6,
          kind:        "followupboss_#{kind}",
          title:       kind.gsub('-', ' ').pluralize.titleize,
          data:        { output: 'graph' }
        )
      end

      dashboard_ids << fub_calls.id
    end

    Dashboard.transaction do
      fub_people = Dashboard.create(
        account: account,
        name:    "Follow Up Boss People"
      )

      fub_people.graphs.first.update_columns(size: 6)

      %w(people-by-day people-by-week people-by-month).each do |kind|
        fub_people.graphs << Graph.new(
          integration: integration,
          size:        6,
          kind:        "followupboss_#{kind}",
          title:       kind.gsub('-', ' ').pluralize.titleize,
          data:        { output: 'graph' }
        )
      end

      dashboard_ids << fub_people.id
    end

    Dashboard.transaction do
      fub_breakdown = Dashboard.create(
        account: account,
        name:    "Follow Up Boss People Breakdown"
      )

      fub_breakdown.graphs.first.update_columns(size: 6)

      fub_breakdown.graphs << Graph.new(
        integration: integration,
        size:        6,
        kind:        "followupboss_people-lifecycle-pie",
        title:       "People Lifecycle",
        data:        { output: 'pie' }
      )

      %w( people-count
          people-lead-count
          people-contact-count
          people-hot-prospect-count
          people-nurture-count
          people-buyer-count
          people-seller-count
          people-pending-count
          people-closed-count
          people-past-client-count
          people-sphere-count
          people-trash-count
        ).each do |kind|
        fub_breakdown.graphs << Graph.new(
          integration: integration,
          size:        2,
          kind:        "followupboss_#{kind}",
          title:       kind.gsub('-', ' ').pluralize.titleize,
          data:        { output: 'graph' }
        )
      end

      dashboard_ids << fub_breakdown.id
    end

    if include_loop
      loop_object = Loop.create(
        account:       account,
        name:          'Follow Up Boss',
        dashboard_ids: dashboard_ids
      )
    end

    UpdateGraphsWorker.perform_async(integration.id)
    loop_object.uuid if include_loop and loop_object
  end
end