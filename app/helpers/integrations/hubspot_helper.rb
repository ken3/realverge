module Integrations::HubspotHelper

  def self.seed_new_connection(account_id, include_loop = true)
    account     = Account.find(account_id)
    integration = account.integrations.find_by_provider('hubspot_integration')
    hubspot_lifecycle = Dashboard.new
    hubspot_history   = Dashboard.new
    hubspot_loop      = Loop.new

    Dashboard.transaction do
      hubspot_lifecycle = Dashboard.create(
        account: account,
        name:    'Hubspot Contact Lifecycle'
      )

      hubspot_lifecycle.graphs.first.update_columns(size: 6)

      hubspot_lifecycle.graphs << Graph.new(
        dashboard:   hubspot_lifecycle,
        integration: integration,
        size:        6,
        kind:        "hubspot_lifecycle-pie-chart",
        title:       "Contacts' Lifecycle Breakdown",
        data:        { output: 'pie' }
      )

      %w(subscriber-count lead-count mql-count sql-count opportunity-count customer-count evangelist-count).each do |kind|
        hubspot_lifecycle.graphs << Graph.new(
          dashboard:   hubspot_lifecycle,
          integration: integration,
          size:        2,
          kind:        "hubspot_#{kind}",
          title:       kind.split('-').first.pluralize.titleize,
          data:        { output: 'number' }
        )
      end
    end

    Dashboard.transaction do
      hubspot_history = Dashboard.create(
        account: account,
        name:    'Hubspot Contact History'
      )

      %w(new-contacts-by-day new-contacts-by-week).each do |kind|
        hubspot_history.graphs << Graph.new(
          dashboard:   hubspot_history,
          integration: integration,
          kind:        "hubspot_#{kind}",
          title:       kind.split('-')[-2..-1].join(' ').pluralize.titleize,
          data:        { output: 'graph' }
        )
      end

      hubspot_history.graphs << Graph.new(
        dashboard:   hubspot_history,
        integration: integration,
        size:        6,
        kind:        "hubspot_new-contacts-by-month",
        title:       'By Month',
        data:        { output: 'graph' }
      )

      hubspot_history.graphs << Graph.new(
        dashboard:   hubspot_history,
        integration: integration,
        size:        6,
        kind:        "hubspot_list-newest-contacts",
        title:       'Newest Contacts',
        data:        { output: 'list' }
      )
    end

    if include_loop
      hubspot_loop = Loop.create(
        account:       account,
        name:          'Hubspot',
        dashboard_ids: [hubspot_lifecycle.id, hubspot_history.id]
      )
    end

    UpdateGraphsWorker.perform_async(integration.id)
    hubspot_loop.uuid if include_loop
  end
end