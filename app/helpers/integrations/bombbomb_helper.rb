module Integrations::BombbombHelper

  def self.seed_new_connection(account_id, include_loop = true)
    account     = Account.find(account_id)
    integration = account.integrations.find_by_provider('bombbomb_integration')
    bombbomb_dashboard = Dashboard.new
    bombbomb_loop      = Loop.new

    emails = integration.get_emails
    lists  = integration.get_lists

    emails_board_ids = []
    lists_board_ids = []

    emails.each do |email|
      Dashboard.transaction do
        bombbomb_dashboard = Dashboard.create(
          account: account,
          name:    "BombBomb #{email[:name]}"
        )

        %w( emails-sent-last-30-days
            emails-sent-last-60-days
            emails-sent-last-90-days
            email-delivered-percent
            email-open-percent
            email-clicks-to-delivered-percent
            email-clicks-to-opens-percent
            email-plays-to-delivered-percent
            email-plays-to-opens-percent
          ).each do |kind|
          bombbomb_dashboard.graphs << Graph.new(
            integration: integration,
            size:        3,
            kind:        "bombbomb_#{kind}",
            title:       kind.gsub('-', ' ').pluralize.titleize,
            data:        { output: 'number', email_id: email[:id] }
          )
        end

        emails_board_ids << bombbomb_dashboard.id
      end
    end

    lists.each do |list|
      Dashboard.transaction do
        bombbomb_dashboard = Dashboard.create(
          account: account,
          name:    "BombBomb #{list[:name]}"
        )

        bombbomb_dashboard.graphs.first.update_columns(size: 6)

        bombbomb_dashboard.graphs << Graph.new(
          integration: integration,
          size:        6,
          kind:        "bombbomb_contacts-in-lists-pie-chart",
          title:       "Contacts' by List",
          data:        { output: 'pie' }
        )

        %w(contacts-in-list-count contacts-in-list-graph-daily contacts-in-list-graph-weekly contacts-in-list-graph-monthly).each do |kind|
          bombbomb_dashboard.graphs << Graph.new(
            dashboard:   bombbomb_dashboard,
            integration: integration,
            size:        3,
            kind:        "bombbomb_#{kind}",
            title:       kind.gsub('-', ' ').pluralize.titleize,
            data:        { output: 'number', list_id: list[:id] }
          )
        end

        lists_board_ids << bombbomb_dashboard.id
      end
    end

    if include_loop
      bombbomb_loop = Loop.create(
        account:       account,
        name:          'BombBomb Emails',
        dashboard_ids: emails_board_ids
      )

      Loop.create(
        account:       account,
        name:          'BombBomb Lists',
        dashboard_ids: lists_board_ids
      )
    end

    UpdateGraphsWorker.perform_async(integration.id)
    bombbomb_loop.uuid if include_loop and bombbomb_loop
  end
end