class LoopChannel < ApplicationCable::Channel
  def subscribed
    stream_from "loop-#{params[:loop_uuid]}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
