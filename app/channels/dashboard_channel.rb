class DashboardChannel < ApplicationCable::Channel
  def subscribed
    stream_from "dashboard-#{params[:dashboard_uuid]}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
