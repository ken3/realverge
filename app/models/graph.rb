class Graph < ApplicationRecord
  acts_as_paranoid

  include Uuid
  include GraphOperation

  PROPERTY_STATUSES = %w(New Active Cancelled Contingent Pending Closed Reduced Withdrawn)
  SIZES = [['Tiny', 2], ['Small', 3], ['Medium', 6], ['Large', 9], ['Entire Dashboard', 12]]
  KINDS = %w(
    bombbomb_contacts-in-lists-pie-chart
    bombbomb_contacts-in-list-count
    bombbomb_contacts-in-list-graph-daily
    bombbomb_contacts-in-list-graph-weekly
    bombbomb_contacts-in-list-graph-monthly
    bombbomb_email-clicks-to-delivered-percent
    bombbomb_email-clicks-to-opens-percent
    bombbomb_email-plays-to-opens-percent
    bombbomb_email-plays-to-delivered-percent
    bombbomb_email-delivered-percent
    bombbomb_email-open-percent
    bombbomb_email-delivered-percent
    bombbomb_emails-sent-last-30-days
    bombbomb_emails-sent-last-60-days
    bombbomb_emails-sent-last-90-days
    facebook_number-of-fans
    facebook_number-of-new-fans-daily
    facebook_engaged-users-daily
    facebook_engaged-users-weekly
    facebook_engaged-users-monthly
    facebook_impressions-daily
    facebook_impressions-weekly
    facebook_impressions-monthly
    followupboss_calls-by-day
    followupboss_calls-by-week
    followupboss_calls-by-month
    followupboss_people-by-day
    followupboss_people-by-week
    followupboss_people-by-month
    followupboss_people-count
    followupboss_people-lead-count
    followupboss_people-contact-count
    followupboss_people-hot-prospect-count
    followupboss_people-nurture-count
    followupboss_people-buyer-count
    followupboss_people-seller-count
    followupboss_people-pending-count
    followupboss_people-closed-count
    followupboss_people-past-client-count
    followupboss_people-sphere-count
    followupboss_people-trash-count
    followupboss_people-lifecycle-pie
    google_bar
    google_currency
    google_donut
    google_line
    google_list
    google_number
    google_pie
    hubspot_lifecycle-pie-chart
    hubspot_subscriber-count
    hubspot_lead-count
    hubspot_mql-count
    hubspot_sql-count
    hubspot_opportunity-count
    hubspot_customer-count
    hubspot_evangelist-count
    hubspot_new-contacts-by-day
    hubspot_new-contacts-by-week
    hubspot_new-contacts-by-month
    hubspot_list-newest-contacts
    native_bar
    native_currency
    native_donut
    native_image
    native_line
    native_list
    native_number
    native_paragraph
    native_pie
    native_title
    zapier_currency
    zapier_number
    zapier_paragraph
    zapier_title
  )

  belongs_to :dashboard, touch: true, counter_cache: true
  belongs_to :integration, optional: true
  has_one :account, through: :dashboard

  validates :size, :title, :kind, presence: true
  validates :size, inclusion: { in: SIZES.map{|m| m[1] }, message: 'not a valid size' }
  validates :title, length: { maximum: 256 }

  before_validation :copy_title, if: 'self.kind == "native_title"'
  after_update :reset_error

  scope :valid, -> { where("error_count <= 10") }

  def self.with_connection(connection)
    KINDS.select{|k| k.include?(connection) }
  end

  def self.kinds_as_object
    kinds = {}
    Integration::PROVIDERS.each do |provider|
      kinds[provider[:provider].split('_').first] = []
    end
    KINDS.each{|k| kinds[k.split('_').first] << k.split('_').last }
    kinds
  end

  private

  def reset_error
    if self.errored_out?
      self.update_columns(error_count: 0, error_msg: '')
    end
  end

  def copy_title
    self.data = { value: self.title, output: 'title' }
  end
end