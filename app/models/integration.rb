require "yaml"

class Integration < ApplicationRecord
  acts_as_paranoid

  include IntegrationOperation

  template  ||= ERB.new(File.new(Rails.root.join('app', 'models', 'providers.yml.erb')).read)
  PROVIDERS ||= YAML.load(template.result(binding))
  NOT_IN_LOOP = ['zapier_integration', 'hubspot_integration', 'google_integration']

  belongs_to :account
  has_many :graphs

  validates :provider, uniqueness: { scope: [:account_id], message: 'this connection has already been enabled' }

  after_initialize   :include_integration_business_logic
  after_find         :include_integration_business_logic
  after_create       :increase_trial!, :integration_uniq_logic
  after_commit       -> { self.account.update_hubspot! }, if: 'Rails.env.production?'

  scope :is_enabled, -> { where(enabled: true) }
  scope :in_loop,    -> { is_enabled.where(account: Account.valid).where.not(provider: NOT_IN_LOOP) }

  def update_graphs!
    Graph.transaction do
      # Touch integration's dashboards if any of the graphs update.
      touch_dashboards! if graphs.valid.map(&:update_data!).any?
    end
  end

  def retrieve(opts)
    case(opts['f'])
    when 'get_data'
      get_data(opts)
    when 'get_spreadsheets'
      get_spreadsheets(opts['q'])
    when 'get_worksheets'
      get_worksheets(opts['spreadsheet_id'])
    else
      self.send(opts['f'].to_s)
    end
  end

  def extract_auth(auth, opts, _)
    self.auth = auth.credentials
  end

  # Update all the dashboards that have glances that use this integration.
  def touch_dashboards!
    Dashboard.where(id: Graph.where(integration_id: id).pluck(:dashboard_id)).each(&:touch)
  end

  def name
    PROVIDERS.select do |pr|
      pr[:provider] == provider
    end[0][:name].capitalize
  end

  private

  def integration_uniq_logic
    return true # Override in integration files as needed.
  end

  def increase_trial!
    # Add a week to their customer's trial (or billing cycle) if it is their first integration.
    self.account.add_week_to_billing_cycle! if self.account.integrations.is_enabled.count == 1
  end

  # Load business logic for spicific integration provider.
  def include_integration_business_logic
    self.send(:extend, "Integrations::#{provider.classify}".constantize)
  end
end