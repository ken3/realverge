module Uuid
  extend ActiveSupport::Concern

  included do
    before_create :generate_uuid
  end

  def regenerate_uuid!
    update_columns(uuid: generate_uuid)
  end

  private

  def generate_uuid
    self.uuid = loop do
      uuid = SecureRandom.uuid
      break uuid unless included_classes.map {|c| c.exists?(uuid: uuid) }.include?(true)
    end
  end

  def included_classes
    ApplicationRecord.descendants.select do |c|
      c.included_modules.include?(Uuid)
    end
  end
end