module Integrations::ZapierIntegration

  def api_token
    auth && auth['api_token']
  end

end