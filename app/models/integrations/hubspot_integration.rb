module Integrations::HubspotIntegration
  BASE_URL = 'https://api.hubapi.com/'

  def extract_auth(_, _, params)
    self.auth = { code: params['code'] }
    refresh_token!
  end

  def normalize_data(d, kind)
    kind = kind.downcase
    case kind
      when 'lifecycle-pie-chart'   then pie_chart
      when 'subscriber-count'      then count(:subscriber)
      when 'lead-count'            then count(:lead)
      when 'mql-count'             then count(:mql)
      when 'sql-count'             then count(:sql)
      when 'opportunity-count'     then count(:opportunity)
      when 'customer-count'        then count(:customer)
      when 'evangelist-count'      then count(:evangelist)
      when 'new-contacts-by-day'   then graph(:day)
      when 'new-contacts-by-week'  then graph(:week)
      when 'new-contacts-by-month' then graph(:month)
      when 'list-newest-contacts'  then list
    end
  end

  def get_data(opts)
    normalize_data(opts, opts['kind'])
  end

  def pie_chart
    update_contacts!
    values = []
    labels = []
    stages = self.auth['contacts'].map{|l| l['lifecyclestage'] }
    stages.each_with_object(Hash.new(0)) do |word, counts|
      counts[word] += 1
    end.each do |k, v|
      labels << k
      values << v
    end
    { labels: labels, series: values, output: 'pie' }
  end

  def count(lifecyclestage)
    update_contacts!
    value = self.auth['contacts'].count{|l| l['lifecyclestage'] == lifecyclestage.to_s }
    { value: value || 0, output: 'number' }
  end

  def graph(duration)
    update_contacts!
    values = []
    labels = []
    start = case duration
      when :day   then Time.current.beginning_of_week
      when :week  then Time.current.beginning_of_month
      when :month then Time.current.beginning_of_year
    end
    loop do
      values << self.auth['contacts'].count{|c| c['created_at'] >= start.at_midnight && (c['created_at'] < start.at_midnight + 1.send(duration)) }
      labels << start.strftime('%b %d')
      start += 1.send(duration)
      break if (start > (-> { Time.now }.call))
    end
    { labels: labels, series: values, output: 'bar' }
  end

  def list
    update_contacts!
    values = self.auth['contacts'].sort do |a, b|
      a['created_at'] <=> b['created_at']
    end.last(10).map{|c| "#{c['first_name']} #{c['last_name']}" }.reverse
    { series: values, output: 'list' }
  end

  private

  def update_contacts!
    # Don't update more than once per minute
    return true if self.auth['updated_at'] && (self.auth['updated_at'] > (Time.current - 1.minute))
    refresh_token!
    self.update_columns(auth: self.auth.merge({
      contacts: get_contacts,
      updated_at: Time.current
    }))
  end

  def get_contacts
    contacts   = []
    vid_offset = nil
    loop do
      # Make API call
      conn = Faraday.new request: { params_encoder: Faraday::FlatParamsEncoder }
      res = conn.get do |req|
        req.url (BASE_URL + '/contacts/v1/lists/all/contacts/all')
        req.params['count'] = 100
        req.params['property'] = ['lifecyclestage', 'firstname', 'lastname']
        req.params['vidOffset'] = vid_offset
        req.headers['Authorization'] = "Bearer #{self.auth['access_token']}"
      end.body

      # Map contacts to simpler data structure
      res = JSON.parse(res)
      vid_offset = res['vid-offset']
      res['contacts'].each do |c|
        contacts << {
          vid: c['vid'],
          created_at: Time.at(c['addedAt'] / 1000),
          lifecyclestage: c['properties']['lifecyclestage']['value'],
          first_name: c['properties']['firstname'] && c['properties']['firstname']['value'] || '',
          last_name: c['properties']['lastname'] && c['properties']['lastname']['value'] || ''
        }
      end
      sleep 0.1 # Hubspot has a max of 10 hits per second per user.
      break unless res['has-more']
    end
    contacts
  end

  def refresh_token!
    return true if self.auth['expires_at'] && (self.auth['expires_at'] > Time.current)

    body = self.auth['refresh_token'].present? ?
      { grant_type: 'refresh_token', refresh_token: self.auth['refresh_token'] } :
      { grant_type: 'authorization_code', code: self.auth['code'] }

    response = Faraday.post(BASE_URL + 'oauth/v1/token', {
      client_id:     ENV['HUBSPOT_CLIENT_ID'],
      client_secret: ENV['HUBSPOT_SECRET_ID'],
      redirect_uri:  "https://#{ENV['DOMAIN']}/integrations/hubspot_integration/callback",
    }.merge(body))

    response_body = JSON.parse(response.body)
    expires_at = response_body['expires_in'] && (response_body['expires_in'] + Time.current.to_i)

    self.auth = self.auth.merge({
      access_token:  response_body['access_token'],
      refresh_token: response_body['refresh_token'],
      expires_at:    Time.at(expires_at),
      portal_id:     get_portal_id(response_body['access_token'])
    })

    self.update_columns(auth: self.auth) if self.persisted?
  end

  def get_portal_id(access_token)
    res = Faraday.get("#{BASE_URL}oauth/v1/access-tokens/#{access_token}")
    res = JSON.parse(res.body)
    res['hub_id']
  end

end