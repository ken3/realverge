require 'google/apis/sheets_v4'
require 'google/apis/drive_v3'
require 'googleauth/client_id'
require 'googleauth/user_authorizer'
require 'net/http'
require 'json'

module Integrations::GoogleIntegration

  def normalize_data(d, kind)
    kind = kind.downcase
    case kind
    when 'number', 'currency'
      { 'value' => get_value(d['sheet_id'], d['range'], d['sheet_name']).flatten.first }
    when 'bar', 'line', 'pie', 'donut'
      # Ignore the first value as it is the sheet row/column header.
      _, *values = *get_value(d['sheet_id'], d['range'], d['sheet_name']).flatten
      _, *labels = *get_labels(d['sheet_id'], d['range_labels'], d['sheet_name']).flatten
      { 'series' => values, 'labels' => labels }
    when 'list'
      { 'series' => get_value(d['sheet_id'], d['range'], d['sheet_name']) }
    end
  end

  def get_data(opts)
    normalize_data(opts, opts['kind'])
  end

  def get_spreadsheets(q = '')
    q = "mimeType = 'application/vnd.google-apps.spreadsheet' and 'me' in owners and fullText contains '#{q}'"
    driveFiles = drive_service.list_files(q: q, options: { grant_type: :reauthorization_code })
    driveFiles.files.map{|f| { id: f.id, name: f.name } }
  end

  def get_worksheets(id)
    worksheet = sheets_service.get_spreadsheet(id)
    worksheet.sheets.select{|s| !s.properties.hidden }.map {|s| s.properties.title }
  end

  def get_label(id, range, sheet_name = '')
    value  = sheets_service.get_spreadsheet_values(id, "#{sheet_name}!#{range}")
    [value.values].flatten.reject(&:blank?)
  end
  alias_method :get_labels, :get_label

  def get_value(id, range, sheet_name = '')
    value  = sheets_service.get_spreadsheet_values(id, "#{sheet_name}!#{range}")
    [value.values].flatten.reject(&:blank?).map{|i| i.scan(/\d+|\./).join.to_f }
  end
  alias_method :get_values, :get_value

  def subscribe_to_webhook
    s = drive_service.watch_change('1', channel_service)
    self.update_columns(auth: self.auth.merge(resource_id: s.resource_id))
  end

  def unsubscribe_from_webhook
    drive_service.stop_channel(channel_service(self.auth['resource_id']))
  end

  private

  def integration_uniq_logic
    self.reload.subscribe_to_webhook
  end

  def channel_service(resource_id = '')
    Google::Apis::DriveV3::Channel.new(
      id: self.id,
      address: "https://#{ENV['DOMAIN']}/api/google/webhook",
      type: "web_hook",
      resource_id: resource_id,
      expiration: (Time.current + 2.days).to_i * 1000
    )
  end

  def drive_service
    drive = Google::Apis::DriveV3::DriveService.new
    drive.client_options.application_name = 'AgentEcho'
    drive.authorization = authorization
    drive
  end

  def sheets_service
    service = Google::Apis::SheetsV4::SheetsService.new
    service.client_options.application_name = 'AgentEcho'
    service.authorization = authorization
    service
  end

  def authorization
    token = Integrations::Token.new(self.auth)
    authorization = Google::Auth::UserAuthorizer.new(
      Google::Auth::ClientId.new(ENV['GOOGLE_CLIENT_ID_INTEGRATION'], ENV['GOOGLE_CLIENT_SECRET_INTEGRATION']),
      [Google::Apis::SheetsV4::AUTH_SPREADSHEETS_READONLY, Google::Apis::SheetsV4::AUTH_DRIVE_READONLY],
      token
    )
    t = authorization.get_credentials(self.id)
    # binding.pry
    t
  end
end

class Integrations::Token
  def initialize(options)
    @url    = options[:uri] || "https://accounts.google.com/o/oauth2/token"
    @params = {
      access_token:  options['access_token']  || options['token'],
      refresh_token: options['refresh_token'],
      client_id:     options['client_id']     || ENV['GOOGLE_CLIENT_ID_INTEGRATION'],
      client_secret: options['client_secret'] || ENV['GOOGLE_CLIENT_SECRET_INTEGRATION'],
      expires_at:    options['expires_at'] || -> { (Time.current + 60.minutes).to_i }.call,
      grant_type:    'refresh_token'
    }
  end

  def store(id, auth)
    current_auth = JSON.parse(load(id))
    expire = current_auth['expiration_time_millis']
    # Exit if the current auth is still valid.
    return current_auth if expire && (expire > -> { Time.current.to_i * 1000 }.call)
    new_auth = JSON.parse(auth)
    new_auth['expiration_time_millis'] = -> { (Time.current + 60.minutes).to_i * 1000 }.call
    Integration.find(id).update_columns(auth: new_auth)
  end

  def load(id)
    Integration.find(id).auth.to_json
  end
end