module Integrations::FollowupbossIntegration
  BASE_URL = 'api.followupboss.com/v1'

  def extract_auth(auth, opts, params)
    self.auth = { api_key: params['auth']['api_key'].strip }
  end

  def normalize_data(d, kind)
    kind = kind.downcase
    case kind
      when 'calls-by-day'              then graph(:calls, { duration: :day })
      when 'calls-by-week'             then graph(:calls, { duration: :week })
      when 'calls-by-month'            then graph(:calls, { duration: :month })
      when 'people-by-day'             then graph(:people, { duration: :day })
      when 'people-by-week'            then graph(:people, { duration: :week })
      when 'people-by-month'           then graph(:people, { duration: :month })
      when 'people-count'              then count(:people)
      when 'people-lead-count'         then count(:people, { stage: 'lead' })
      when 'people-contact-count'      then count(:people, { stage: 'contact' })
      when 'people-hot-prospect-count' then count(:people, { stage: 'hot prospect' })
      when 'people-nurture-count'      then count(:people, { stage: 'nurture' })
      when 'people-buyer-count'        then count(:people, { stage: 'buyer' })
      when 'people-seller-count'       then count(:people, { stage: 'seller' })
      when 'people-pending-count'      then count(:people, { stage: 'pending' })
      when 'people-closed-count'       then count(:people, { stage: 'closed' })
      when 'people-past-client-count'  then count(:people, { stage: 'past client' })
      when 'people-sphere-count'       then count(:people, { stage: 'sphere' })
      when 'people-trash-count'        then count(:people, { stage: 'trash' })
      when 'people-lifecycle-pie'      then pie(:people, { stages: ['lead', 'contact', 'hot prospect', 'nurture'] })
    end
  end

  def get_data(opts)
    normalize_data(opts, opts['kind'])
  end

  def graph(path, opts)
    values = []
    labels = []
    start = case opts[:duration]
      when :day   then Time.current.beginning_of_week
      when :week  then Time.current.beginning_of_month
      when :month then Time.current.beginning_of_year
    end
    loop do
      values << pull_data(path, { createdAfter: start, createdBefore: start + 1.send(opts[:duration]) })['total']
      labels << start.strftime('%b %d')
      start += 1.send(opts[:duration])
      break if (start > (-> { Time.now }.call))
    end
    { labels: labels, series: values, output: 'bar' }
  end

  def pie(path, opts = {})
    {
      series: opts[:stages].map{|s| pull_data(path, { stage: s })['total'] },
      labels: opts[:stages],
      output: 'pie'
    }
  end

  def count(path, opts = {})
    { value: pull_data(path.to_s, opts)['total'], output: 'number' }
  end

  private

  def pull_data(path = '/', opts = {})
    default_params = { limit: 1, system: 'agentecho' }
    conn = Faraday.new("https://#{self.auth['api_key']}:@#{BASE_URL}/#{path}")
    conn.params = default_params.merge(opts)
    JSON.parse(conn.get.body)['_metadata']
  end

end