module Integrations::FacebookIntegration

  def normalize_data(d, kind)
    kind = kind.downcase
    case kind
    when 'number-of-fans'
      { 'value' => number_of_fans(d['page_id']) }
    when 'number-of-new-fans-daily'
      { 'value' => number_of_new_fans_daily(d['page_id']) }
    when 'engaged-users-daily'
      { 'value' => engaged_users_daily(d['page_id']) }
    when 'engaged-users-weekly'
      { 'value' => engaged_users_weekly(d['page_id']) }
    when 'engaged-users-monthly'
      { 'value' => engaged_users_monthly(d['page_id']) }
    when 'impressions-daily'
      { 'value' => impressions_daily(d['page_id']) }
    when 'impressions-weekly'
      { 'value' => impressions_weekly(d['page_id']) }
    when 'impressions-monthly'
      { 'value' => impressions_monthly(d['page_id']) }
    end
  end

  def get_data(opts)
    normalize_data(opts, opts['kind'])
  end

  def get_pages
    refresh_facebook_token!
    auth  = facebook_authorization
    pages = auth.get_connections('me', 'accounts')
    # pages.unshift(auth.get_object('me')) Remove support for personal Facebook page for now.
    pages.map{ |page| { name: page['name'], id: page['id'] } }
  end

  def number_of_fans(page_id)
    graph = page_object(page_id)
    value = graph.get_connection('me', 'insights/page_fans')
    value.first['values'].first['value']
  end

  def number_of_new_fans_daily(page_id)
    graph = page_object(page_id)
    value = graph.get_connection('me', 'insights/page_fan_adds')
    value.first['values'].first['value']
  end

  def engaged_users_daily(page_id, period = :day)
    graph = page_object(page_id)
    value = graph.get_connection('me', 'insights/page_engaged_users', period: period)
    value.first['values'].last['value']
  end

  def engaged_users_weekly(page_id)
    engaged_users_daily(page_id, :week)
  end

  def engaged_users_monthly(page_id)
    engaged_users_daily(page_id, :days_28)
  end

  def impressions_daily(page_id, period = :day)
    graph = page_object(page_id)
    value = graph.get_connection('me', 'insights/page_impressions', period: period)
    value.first['values'].last['value']
  end

  def impressions_weekly(page_id)
    impressions_daily(page_id, :week)
  end

  def impressions_monthly(page_id)
    impressions_daily(page_id, :days_28)
  end

  private

  def page_object(page_id)
    refresh_facebook_token!
    page = facebook_authorization.get_object(page_id, fields: :access_token)
    Koala::Facebook::API.new(page['access_token'])
  end

  def facebook_authorization
    refresh_facebook_token!
    Koala::Facebook::API.new(self.auth['token'])
  end

  def refresh_facebook_token!
    current_time = -> { Time.now.to_i }.call

    if self.auth['expires_at'] && (self.auth['expires_at'] > current_time)
      oauth = Koala::Facebook::OAuth.new(
        ENV['FACEBOOK_APP_ID'],
        ENV['FACEBOOK_APP_SECRET'],
        'https://app.agentecho.io/integrations/facebook'
      )
      new_token = oauth.exchange_access_token_info(self.auth['token'])
      new_token = {
        token: new_token['access_token'],
        expires_at: current_time + new_token[:expires].to_i,
        expires: true
      }
      self.update_columns(auth: new_token)
    end
  end
end