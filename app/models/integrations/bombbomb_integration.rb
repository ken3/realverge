module Integrations::BombbombIntegration
  BASE_URL = 'https://app.bombbomb.com/app/api/api.php'

  def extract_auth(auth, opts, params)
    self.auth = { api_key: params['auth']['api_key'].strip }
  end

  def normalize_data(d, kind)
    kind = kind.downcase
    case kind
      when 'contacts-in-lists-pie-chart'       then pie('GetListContacts')
      when 'contacts-in-list-count'            then count('GetListContacts', { list_id: d['list_id'] })
      when 'contacts-in-list-graph-daily'      then graph('GetListContacts', { list_id: d['list_id'], duration: :day })
      when 'contacts-in-list-graph-weekly'     then graph('GetListContacts', { list_id: d['list_id'], duration: :week })
      when 'contacts-in-list-graph-monthly'    then graph('GetListContacts', { list_id: d['list_id'], duration: :month })
      when 'email-clicks-to-delivered-percent' then rate('emailTracking', { email_id: d['email_id'], stat: 'clicks_to_delivered_percent' })
      when 'email-clicks-to-opens-percent'     then rate('emailTracking', { email_id: d['email_id'], stat: 'clicks_to_opens_percent' })
      when 'email-plays-to-opens-percent'      then rate('emailTracking', { email_id: d['email_id'], stat: 'plays_to_opens_percent' })
      when 'email-plays-to-delivered-percent'  then rate('emailTracking', { email_id: d['email_id'], stat: 'plays_to_delivered_percent' })
      when 'email-delivered-percent'           then rate('emailTracking', { email_id: d['email_id'], stat: 'delivered_percent' })
      when 'email-open-percent'                then rate('emailTracking', { email_id: d['email_id'], stat: 'opens_percent' })
      when 'emails-sent-last-30-days'          then stat_count('emailTracking', { email_id: d['email_id'], stat: 'sends_last_30' })
      when 'emails-sent-last-60-days'          then stat_count('emailTracking', { email_id: d['email_id'], stat: 'sends_last_60' })
      when 'emails-sent-last-90-days'          then stat_count('emailTracking', { email_id: d['email_id'], stat: 'sends_last_90' })
    end
  end

  def get_data(opts)
    normalize_data(opts, opts['kind'])
  end

  def get_lists
    pull_data('GetLists')['info'].map do |list|
      { id: list['id'], name: list['name'] }
    end
  end

  def get_emails
    pull_data('GetEmails')['info'].map do |list|
      { id: list['id'], name: list['name'] }
    end
  end

  def graph(method, opts)
    values = []
    labels = []
    contacts = pull_data(method, opts)['info']
    start = case opts[:duration]
      when :day   then Time.current.beginning_of_week
      when :week  then Time.current.beginning_of_month
      when :month then Time.current.beginning_of_year
    end
    loop do
      values << contacts.count{|c| c['add_date'] > start and c['add_date'] <= start + 1.send(opts[:duration]) }
      labels << start.strftime('%b %d')
      start += 1.send(opts[:duration])
      break if (start > (-> { Time.now }.call))
    end
    { labels: labels, series: values, output: 'bar' }
  end

  def pie(method, opts = {})
    lists = get_lists.map{|l| { name: l[:name], values: pull_data(method, list_id: l[:id])['info'] } }.select{|l| !l[:values].length.zero? }
    {
      series: lists.map{|l| l[:values].length },
      labels: lists.map{|l| l[:name] },
      output: 'pie'
    }
  end

  def rate(method, opts = {})
    stats = pull_data(method, opts.merge({ action: 'getAggregateStatsForAllJobs' }))
    stats = JSON.parse(stats['info'])
    { value: "#{stats[opts[:stat]]}%", output: 'percent' }
  end

  def stat_count(method, opts = {})
    stats = pull_data(method, opts.merge({ action: 'getAggregateStatsForAllJobs' }))
    stats = JSON.parse(stats['info'])
    { value: stats[opts[:stat]], output: 'number' }
  end

  def count(method, opts = {})
    { value: pull_data(method.to_s, opts)['info'].length, output: 'number' }
  end

  private

  def pull_data(method, opts = {})
    default_params = { api_key: self.auth['api_key'], method: method }
    conn = Faraday.new(BASE_URL)
    conn.params = default_params.merge(opts)
    JSON.parse(conn.get.body)
  end

end
