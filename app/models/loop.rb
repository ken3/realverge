class Loop < ApplicationRecord
  acts_as_paranoid

  include Uuid

  DURATIONS   = [10, 30, 120, 600]
  TRANSITIONS = %w(default none fade slide convex concave zoom)

  belongs_to :account, touch: true

  validates :name,       presence: true, length:    { maximum: 256 }
  validates :duration,   presence: true, inclusion: { in: DURATIONS }
  validates :transition, presence: true, inclusion: { in: TRANSITIONS }
  validate  :two_dashboards

  after_update :update_client_loop

  def dashboards
    self.account.dashboards.find(self.dashboard_ids)
  end

  def remove_dashboard(id)
    self.update_columns(dashboard_ids: (self.dashboard_ids - id))
  end

  def dashboard_count
    self.dashboard_ids && self.dashboard_ids.count
  end

  def disable_open_boards!
    return true if self.public_loop?
    self.dashboard_ids.each do |id|
      self.account.dashboards.find(id).disable_open_boards!
    end
  end

  private

  def two_dashboards
    if self.dashboard_ids.length < 1
      self.errors.add(:dashboards, 'must include at least two dashboards')
    end
  end

  def update_client_loop
    LoopBroadcast.perform_in(1.second, self.uuid)
  end
end