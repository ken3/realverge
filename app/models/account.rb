class Account < ApplicationRecord
  has_secure_password
  acts_as_paranoid

  include AccountHelper
  include AccountOperation

  PERSONAS = [
    ['New Agent (0 to 2 years experience)',                   'persona_1'],
    ['Sophomore Agent (2 to 5 years experience)',             'persona_2'],
    ['Established Agent (6 or more years of experience)',     'persona_3'],
    ['New Team Owner (1 to 2 years owning your team)',        'persona_4'],
    ['Experienced Team Owner (more than 3 years experience)', 'persona_5']
  ]
  PLANS = {
    free:      { name: 'Free',   stripe_id: 0, price: 0  },
    agent:     { name: 'Agent',  stripe_id: 1, price: 9  },
    agentplus: { name: 'Agent+', stripe_id: 2, price: 49 },
    team:      { name: 'Team',   stripe_id: 3, price: 99 }
  }

  has_many :dashboards, dependent: :destroy
  has_many :loops, dependent: :destroy
  has_many :graphs, through: :dashboards
  has_many :integrations, dependent: :destroy
  has_many :referrals, foreign_key: :referrer_id, class_name: 'Account'

  belongs_to :referrer, counter_cache: :referrals_count, class_name: 'Account'

  before_create :generate_api_key
  after_create :seed_data
  after_create :new_account_jobs, :benefit_referrer, if: 'Rails.env.production?'
  after_update :increase_trial!
  after_update :update_zendesk!, :update_hubspot!, :update_stripe_plan, if: 'Rails.env.production?'

  validate  :email_uniqueness, on: :create
  validates :name, :email, presence: true, length: { maximum: 256 }
  validates :phone, length: { maximum: 30 }

  scope :valid, -> { where(billing_issue: false) }

  def self.from_omniauth(auth, opts)
    return false unless auth.present?
    expires = auth.credentials.expires_at.nil? ? '' : Time.at(auth.credentials.expires_at)
    account = with_deleted.where(provider: auth.provider, uid: auth.uid, email: auth.info.email).first_or_initialize(
      name:             auth.info.name,
      password:         SecureRandom.urlsafe_base64,
      oauth_token:      auth.credentials.token,
      oauth_expires_at: expires,
      source:           opts["state"].andand["source"],
      referrer_id:      opts["state"].andand["referrer"]
    )
    account.save if account.new_record?
    account
  end

  private

  def update_stripe_plan
    self.change_plan!(self.plan) if self.plan_changed?
  end

  def increase_trial!
    # Add a week to their customer's trial (or billing cycle) if they set their persona.
    self.add_week_to_billing_cycle! if self.persona_changed? and self.persona_was == nil
  end

  def generate_api_key
    self.api_token = loop do
      token = SecureRandom.hex
      break token unless Account.exists?(api_token: token)
    end
  end

  def benefit_referrer
    self.referrer.add_week_to_billing_cycle! unless self.referrer.nil?
  end

  def email_uniqueness
    if Account.with_deleted.exists?(email: email)
      errors.add(:email, "an account already exists with this email address")
    end
  end

  def seed_data
    seed_new_account(self.id)
  end

  def new_account_jobs
    AccountCreation.perform_in(1.second, self.id)
  end
end