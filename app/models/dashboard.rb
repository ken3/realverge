class Dashboard < ApplicationRecord
  acts_as_paranoid

  include Uuid
  include DashboardOperation

  belongs_to :account, touch: true
  has_many :graphs, dependent: :destroy

  validates :name, presence: true, length: { maximum: 256 }

  after_create :add_title_graph
  after_touch  :update_client_dashboard
  after_update :update_client_dashboard

  def loop_object
    return true
  end

  private

  def add_title_graph
    default_graph = Graph.new(
      dashboard: self,
      kind: 'native_title',
      title: self.name,
      data: { value: self.name, output: 'title' },
      size: 3
    )
    default_graph.save!
  end

  def update_client_dashboard
    DashboardBroadcast.perform_in(1.second, self.uuid)
  end
end