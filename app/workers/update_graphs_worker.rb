class UpdateGraphsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(integration_id)
    integration = Integration.find(integration_id)
    integration.update_graphs!
  end
end