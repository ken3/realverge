class PasswordReset
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(account_id)
    account = Account.find(account_id)

    link = Rails.application.routes.url_helpers.password_url(
      token: account.password_reset_token,
      protocol: :https,
      host: 'app.agentecho.io'
    )

    message = {
      merge_vars: [{
        rcpt: account.email,
        vars: [{
          content: "<a href='#{link}' target='_blank'>Click here to upate your password.</a>",
          name: 'RESET_URL'
        }]
      }],
      to: [{
        type: :to,
        email: account.email,
        name: account.name
      }]
    }

    $mandrill.messages.send_template 'password-reset', [], message
  end
end