class GoogleWebhookSubscription
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    Integration.where(provider: 'google_integration', account: Account.valid).each(&:subscribe_to_webhook)
  end
end
