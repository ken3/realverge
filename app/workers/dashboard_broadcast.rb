class DashboardBroadcast
  include Sidekiq::Worker
  include ViewHelper
  sidekiq_options retry: 3

  def perform(dashboard_uuid)
    if(dashboard = Dashboard.where(uuid: dashboard_uuid).first)
      ActionCable.server.broadcast "dashboard-#{dashboard.uuid}", {
        version: ENV['APP_VERSION'],
        uuid: dashboard.uuid,
        glances: { glances: dashboard.graphs_position_order.map(&:dashboard_data) }.to_json
      }
    end
  end
end
