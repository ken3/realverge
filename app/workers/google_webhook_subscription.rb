class AccountCreation
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(account_id)
    account = Account.find(account_id)

    # Add account to Zendesk
    account.update_zendesk!

    # Add account to Hubspot
    account.update_hubspot!

    # Add account to Stripe
    account.create_stripe!

    logger.info "=== New Account Created | Name: #{account.name} | Email: #{account.email} | Phone: #{account.phone}"
  end
end
