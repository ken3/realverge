class UpdateLoopWorker
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: 'loop'

  def perform
    # Clear any waiting jobs in loop.
    Sidekiq::ScheduledSet.new.clear

    # Do all the things.
    Integration.in_loop.pluck(:id).each do |id|
      UpdateGraphsWorker.perform_async(id)
    end

    interval = Rails.env.development? ?
                 20.seconds :
                 (ENV['LOOP_INTERVAL'] && ENV['LOOP_INTERVAL'].to_i.seconds) || 120.seconds

    # Put the next run on the queue.
    self.class.perform_in(interval)
  end
end