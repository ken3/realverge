class ZendeskUpdate
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: 'low'

  def perform(account_id)
    account = Account.find(account_id)
    account_data = {
      name: account.name,
      email: account.email,
      time_zone: account.time_zone,
      phone: account.phone,
      role: "end-user"
    }

    # Get user if they are already in Zendesk
    if(account.zendesk_id.present? and (user = $zendesk.users.find!(id: account.zendesk_id)))
      user.attributes.merge!(account_data)
      user.save!
    else
      user = $zendesk.users.create!(account_data)
      account.update_columns(zendesk_id: user.id)
    end
  end
end
