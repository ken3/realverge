class EngagementUpdate
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: 'low'

  def perform
    Sidekiq::ScheduledSet.new.clear
    Account.all.each(&:update_hubspot!)
    self.class.perform_at((Date.today + 1.day).at_midnight)
  end
end
