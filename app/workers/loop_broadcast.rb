class LoopBroadcast
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(loop_uuid)
    if(loop_object = Loop.find_by_uuid(loop_uuid))
      ActionCable.server.broadcast "loop-#{loop_object.uuid}", {
        uuid: loop_object.uuid,
        dashboard_uuids: loop_object.dashboards.pluck(:uuid),
        duration: loop_object.duration,
        transition: loop_object.transition
      }
    end
  end
end
