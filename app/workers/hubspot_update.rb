class HubspotUpdate
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: 'low', rate: { name: 'hubspot', limit: 4, period: 1.second }

  def perform(account_id)
    account = Account.find(account_id)

    account_data = {
      firstname: account.firstname,
      lastname: account.lastname,
      phone: account.phone,
      lifecyclestage: account.paid? ? 'customer' : 'opportunity'
    }

    account_integrations = account.integrations.is_enabled.map(&:provider)
    Integration::PROVIDERS.select{|i| i[:provider] != 'native_integration'}.map do |i|
      account_data[i[:provider]] = account_integrations.include?(i[:provider])
    end

    account_data['engaged_account']    = account_integrations.any?
    account_data['hs_persona']         = account.persona
    account_data['persona_completed']  = !account.persona.blank?
    account_data['last_seen_on']       = account.last_seen_at.utc.at_midnight.to_i * 1000
    account_data['billing_cycle_ends'] = account.cycle_ends_on.utc.at_midnight.to_i * 1000
    account_data['on_trial']           = account.on_trial?
    account_data['referral_count']     = account.referrals_count

    account_data['lifetime_engagement_points']       = account.engagement_points
    account_data['weekly_average_engagement_points'] = account.weekly_average_engagement_points

    # Get contact if they are already in Hubspot
    if(account.hubspot_id.present? and (contact = Hubspot::Contact.find_by_id(account.hubspot_id.to_i)))
      contact.update!(account_data)
    else
      # Use create or update in case the user has been added to the CRM via other avenues.
      contact = Hubspot::Contact.createOrUpdate(account.email, account_data)
      account.update_columns(hubspot_id: contact.vid)
    end
  end
end