module DashboardOperation

  def graphs_position_order
    graphs.order("position(','||id::text||',' IN ',#{positions.join(',')},')").order(created_at: :asc)
  end

  def disable_open_boards!
    DashboardBroadcast.perform_in(1.second, self.uuid, disable: true) unless !public_form
  end

  def duplicate!(newName = '')
    new_board = nil
    Dashboard.skip_callback(:create, :after, :add_title_graph)
    transaction do
      new_board = dup
      new_board.name = newName.blank? ? name : newName
      new_board.graphs_count = 0
      if new_board.save
        graphs_position_order.each do |graph|
          new_board.graphs << graph.dup
        end
      end
    end
    Dashboard.set_callback(:create, :after, :add_title_graph)
    new_board
  end
end