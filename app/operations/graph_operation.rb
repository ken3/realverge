module GraphOperation
  extend ActiveSupport::Concern

  def google?
    integration_kind == 'google'
  end

  def facebook?
    integration_kind == 'facebook'
  end

  def native?
    integration_kind == 'native'
  end

  def zapier?
    integration_kind == 'zapier'
  end

  def integration_kind
    kind.split('_').try(:first)
  end

  def graph_kind
    kind.split('_').try(:last)
  end

  def dashboard_data
    slice(:id, :kind, :size, :title, :data)
  end

  def output
    data && data['output'] || graph_kind.split('_').last
  end

  def errored_out?
    self.error_count >= 10
  end

  def update_data!
    old_data = self.read_attribute(:data) || {}
    begin
      new_data = self.integration.normalize_data(old_data, graph_kind)
      new_data = old_data.deep_merge(new_data)
      if new_data != old_data
        self.update_columns(data: new_data)
        self.account.points!
        logger.info "Updated Glance ID-#{self.id} with: #{new_data.inspect}"
      end
      self.update_columns(error_count: 0, error_msg: '') unless self.error_count.zero?
      return new_data != old_data
    rescue
      self.increment!(:error_count)
      self.update_columns(error_msg: $!.message)
      self.touch if self.reload.errored_out?
      logger.error "Error when updating Glance ID-#{self.id}. Consecutive error count: #{self.error_count}. Message: #{self.error_msg}"
    end
  end

  def week
    return [] if data.nil?
    iEnd = data['series'][0].length - 1
    wday = Time.current.wday - 1
    custom_range(self.data, iEnd, wday)
  end

  def month
    return [] if data.nil?
    iEnd = data['series'][0].length - 1
    mday = Time.current.mday - 1
    custom_range(self.data, iEnd, mday)
  end

  def data=(value)
    data = read_attribute(:data) || {}
    write_attribute(:data, data.merge!(value))
  end
end