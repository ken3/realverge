require "stripe"

module AccountOperation
  def zapier_api_token
    self.integrations.where(provider: 'zapier_integration').first.api_token
  end

  def firstname
    firstname, *_ = *name.strip.split(' ')
    firstname
  end

  def lastname
    _, *lastname = *name.strip.split(' ')
    lastname.any? ? lastname.join(' ') : ''
  end

  def referral_code
    Base64.urlsafe_encode64(self.id.to_s)
  end

  def regenerate_api_key!
    update_columns(api_token: generate_api_key)
  end

  def integration_providers
    ips ||= self.integrations.is_enabled
    Integration::PROVIDERS.sort{|a, b| a[:name] <=> b[:name] }.map do |int|
      int.merge({
        enabled: ips.map(&:provider).include?(int[:provider]),
        integration: ips.find{|i| i.provider == int[:provider] }
      })
    end
  end

  def add_card!(stripe_source)
    stripe_customer.sources.create(source: stripe_source)
  end

  def current_plan_price
    "$#{Account::PLANS[plan.to_sym][:price]}"
  end

  def update_billing_cycle!(date = nil)
    return false unless self.try(:stripe_id)
    date = date.nil? ? (Time.current + 31.days).utc.to_i : date.to_i
    customer               = stripe_customer
    subscription           = (customer.subscriptions.any? && customer.subscriptions.first)
    subscription.trial_end = date
    subscription.prorate   = false
    if subscription.save
      self.update_columns(billing_issue: false, cycle_ends_on: Time.at(date).to_datetime)
      self.update_hubspot!
    end
  end

  def add_week_to_billing_cycle!(weeks = 1)
    return false unless self.try(:stripe_id)
    subscription     = stripe_subscription
    new_invoice_date = Time.at(subscription.trial_end).to_datetime + weeks.weeks
    update_billing_cycle!(new_invoice_date)
  end

  def change_plan!(plan)
    return false unless self.try(:stripe_id)
    plan                   = plan.to_sym
    subscription           = stripe_subscription
    subscription.plan      = Account::PLANS[plan][:stripe_id]
    subscription.trial_end = subscription.trial_end # Don't change trial end date
    if subscription.save
      self.update_columns(plan: plan.to_sym, billing_issue: false)
      self.update_hubspot!
    end
  end

  def failed_billing!(msg = 'Unknown, please contact support')
    self.update_columns(billing_issue: true, billing_issue_msg: msg)
    self.update_hubspot!
  end

  def trial_left
    return self.cycle_ends_on.present? ?
      (Time.parse(self.cycle_ends_on.to_s).to_date - Time.current.to_date).to_i :
      0
  end

  def on_trial?
    self.trial_left > 0
  end

  def paid?
    !self.on_trial? && self.plan?(:free)
  end

  def plan?(plan)
    self.plan.to_sym == plan.to_sym
  end

  def basic_providers
    integration_providers.select{ |pr| pr[:kind] == :basic }
  end

  def enabled_integrations?
    self.integrations.is_enabled.any?
  end

  def persona_selected?
    self.persona.present?
  end

  def premium_providers
    integration_providers.select{ |pr| pr[:kind] == :premium }
  end

  def points!(points = 1)
    self.increment!(:engagement_points, points)
  end

  def weekly_average_engagement_points
    start = self.created_at.to_date < Date.parse('2016-10-17') ?
              Date.parse('2016-10-17') :
              self.created_at.to_date
    weeks = (Date.today - start).to_i.days / 1.weeks
    engagement_points = engagement_points.to_f / weeks.to_f
    engagement_points.nan? ? 0.0 : engagement_points.floor
  end

  def reset_password!
    update_columns(password_reset_token: SecureRandom.urlsafe_base64, reset_password_date: -> { DateTime.now }.call)
    PasswordReset.perform_in(1.second, self.id)
  end

  def update_last_login_at
    update_column(:last_login_at, -> { ApplicationHelper::agentecho_time }.call)
  end

  def update_zendesk!
    ZendeskUpdate.perform_in(1.second, self.id)
  end

  def update_hubspot!
    HubspotUpdate.perform_in(1.second, self.id)
  end

  def create_stripe!
    Stripe.api_key = ENV['STRIPE_SECRET']
    customer = Stripe::Customer.create(
      email:    self.email,
      plan:     Account::PLANS[self.plan.to_sym][:stripe_id],
      metadata: { name: self.name }
    )
    self.update_columns(stripe_id: customer.id)
  end

  def stripe_customer
    return false unless self.try(:stripe_id)
    Stripe.api_key = ENV['STRIPE_SECRET']
    Stripe::Customer.retrieve(self.stripe_id)
  end

  def stripe_subscription
    return false unless self.try(:stripe_id)
    customer = stripe_customer
    (customer.subscriptions.any? && customer.subscriptions.first)
  end
end