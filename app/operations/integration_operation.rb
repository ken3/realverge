module IntegrationOperation

  def self.generate_zapier!(account)
    token = loop do
      token = SecureRandom.hex
      break token unless Integration.where("integrations.auth->>'api_token' = '#{token}'").any?
    end
    Integration.create!(
      account:  account,
      auth:     { api_token: token },
      provider: 'zapier_integration',
      enabled:  false
    )
  end
end