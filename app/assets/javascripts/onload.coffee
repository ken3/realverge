# Delegations and events that need to build on page load.
document.addEventListener 'turbolinks:load', ->
  initMaterialize()
  initSorting()
  initValidation()
  initDateLabels()
  initStripe()
  openWaitingModal()
  initLoop(->
    ReactRailsUJS.mountComponents()
    initFitText()
    initMarquee()
    return
  )