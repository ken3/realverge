pathname = window.location.pathname
if _.includes(pathname.split('/'), 'board') || _.includes(pathname.split('/'), 'loop')
  $.each($('#loop').data('dashboard-uuids'), (_, uuid) ->
    App.dashboard = App.cable.subscriptions.create { channel: "DashboardChannel", dashboard_uuid: uuid },
      connected: ->
        console.log('connected')

      disconnected: ->
        console.log('disconnecting...')
        location.reload()

      received: (data) ->
        console.log('updating')
        if data.version != $('#version')?.data('version')?.toString()
          location.reload()
        else
          $(".dashboard-#{data['uuid']}").attr('data-react-props', data['glances'])
          ReactRailsUJS.mountComponents()
          initFitText()
          initMarquee()
        return
  )