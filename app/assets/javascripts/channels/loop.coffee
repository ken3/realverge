pathname = window.location.pathname
if _.includes(pathname.split('/'), 'loop')
  uuid = $('#loop').data('uuid')
  App.dashboard = App.cable.subscriptions.create { channel: "LoopChannel", loop_uuid: uuid },
    connected: ->
      console.log('connected')

    disconnected: ->
      console.log('disconnecting...')
      location.reload()

    received: (data) ->
      console.log('updating')
      $("#loop").attr('data-duration', data['duration']).attr('data-transition', data['transition'])
      if _.isEqual($("#loop").data('dashboard-uuids'), data['dashboard_uuids'])
        reinitLoop(->
          ReactRailsUJS.mountComponents()
          initFitText()
          initMarquee()
          return
        )
      else
        location.reload()
      return