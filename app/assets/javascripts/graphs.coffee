document.addEventListener 'turbolinks:load', ->
  update_ratios()
  $(window).on 'resize', () ->
    update_ratios()

$(document).on 'click', '.copy-btn', (e) ->
  e.preventDefault()
  link_container = $('.copy-source', $(this).closest('.row'))
  link_container = $('.copy-source', $(this).closest('.modal')) unless !!link_container
  $temp = $("<input>")
  $("body").append($temp)
  $temp.val($(link_container).val()).select()
  document.execCommand("copy")
  $temp.remove()