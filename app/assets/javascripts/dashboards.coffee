$(document).on 'click', '.remove-line', () ->
  $(this).closest('li').remove()

$(document).on 'click', '.copy-link', (e) ->
  e.preventDefault()
  link_container = $('input', $(this).closest('.modal'))
  $temp = $("<input>")
  $("body").append($temp)
  $temp.val($(link_container).val()).select()
  document.execCommand("copy")
  $temp.remove()

$(document).on 'click', '.add-line', () ->
  position = $($(this).closest('li'))
  new_line = $($(this).closest('ul').find('.line-template')).clone()
  # Copy entered data
  $('#graph_data_labels_', new_line).val($('#graph_data_labels_', position).val())
  $('#graph_data_series_', new_line).val($('#graph_data_series_', position).val())
  # Insert the new line with copied data
  new_line.insertBefore(position).removeClass('hide').removeClass('line-template')
  # Clear the position line
  $('#graph_data_labels_', position).val('')
  $('#graph_data_series_', position).val('')
  Materialize.updateTextFields()
  initSorting()
  return

$(document).on 'click', '.add-line-with-date', () ->
  new_line = $($(this).closest('ul').find('.line-template')).clone()

  if $(this).data('persisted')
    position = $('li:first-child', $(this).closest('ul'))
    new_date = moment.unix($('input[type="hidden"]', position).val()).add(1, 'day').utcOffset(TZ)
    $('label', new_line).text($('label', position).text())
    $('input[type="hidden"]', new_line).val($('input[type="hidden"]', position).val())
    $('input[type="number"]', new_line).val($('input[type="number"]', position).val())
    $('label', position).text(new_date.format('MMM Do'))
    $('input[type="hidden"]', position).val(new_date.unix())
    $('input[type="number"]', position).val('')
  else
    position = $('li:nth-last-child(2)', $(this).closest('ul'))
    new_date = moment.unix($('input[type="hidden"]', position).val()).subtract(1, 'day').utcOffset(TZ)
    $('label', new_line).text(new_date.format('MMM Do'))
    $('input[type="hidden"]', new_line).val(new_date.unix())

  new_line.insertAfter(position).removeClass('hide').removeClass('line-template')
  Materialize.updateTextFields()
  return
