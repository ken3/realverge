/* global _ React $ GlanceLine */
var GlanceDonut = React.createClass({

  propTypes: {
    data: React.PropTypes.object
  },

  render: function() {
    return (
      <GlancePie {...this.props} donut />
    )
  }
})