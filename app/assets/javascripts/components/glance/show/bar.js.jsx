/* global _ React $ GlanceLine foramttedValue */
var GlanceBar = React.createClass({

  propTypes: {
    data: React.PropTypes.object
  },

  render: function() {

    var data = {
      labels: this.props.data.labels || [],
      series: [
        this.props.data.series || []
      ]
    }

    var options = {
      low: 0,
      axisY: {
        labelInterpolationFnc: function(value) {
          return foramttedValue(value);
        }
      }
    }

    return (
      <ChartistGraph data={data} options={options} type='Bar' />
    )
  }
})