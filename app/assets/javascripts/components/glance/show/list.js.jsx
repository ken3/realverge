/* global _ React $ GlanceList */
var GlanceList = React.createClass({

  propTypes: {
    data: React.PropTypes.object
  },

  render: function() {
    var series = this.props.data && this.props.data.series
    return (
      <div className='marquee-container'>
        <div className='marquee-placeholder'>
          <ul className='collection'>
            {_.map(series, function(item, i) {
              return <li key={i} className='collection-item'>{item}</li>
            })}
          </ul>
        </div>
      </div>
    )
  }
})