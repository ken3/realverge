/* global GlancePie _ React $ foramttedValue */
var GlancePie = React.createClass({

  propTypes: {
    data: React.PropTypes.object,
    donut: React.PropTypes.boolean
  },

  crunch: function (labels, values) {
    return _.filter(_.map(labels, function(l, i) {
      return l.cap() + ' (' + foramttedValue(values[i]) + ')'
    }), function (_, i) {
      return values[i] != 0
    })
  },

  render: function() {
    var labels = this.crunch(this.props.data.labels, this.props.data.series) || []
    var series = _.compact(this.props.data.series) || []
    var data = { labels: labels, series: series }
    var options = { donut: this.props.donut }
    return (
      <ChartistGraph data={data} options={options} type='Pie' />
    )
  }
})