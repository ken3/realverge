/* global _ React $ GlanceNumber */
var GlanceParagraph = React.createClass({

  propTypes: {
    data: React.PropTypes.object
  },

  render: function() {
    var value = this.props.data && this.props.data.value
    value = value.replace('\n', '<br />')
    return (
      <div className='flow-text' dangerouslySetInnerHTML={{ __html: value }} />
    )
  }
})