/* global _ React $ GlanceLine foramttedValue */
var GlanceLine = React.createClass({

  propTypes: {
    data: React.PropTypes.object
  },

  render: function() {

    var data = {
      labels: this.props.data.labels || [],
      series: [
        this.props.data.series || []
      ]
    }

    var high = Math.max(..._.map(this.props.data.series, function (i) {
      return parseInt(i)
    }))

    var options = {
      high: ((Math.floor(parseInt(high) / 10) * 10) + 10),
      low: 0,
      lineSmooth: false,
      fullWidth: true,
      axisY: {
        labelInterpolationFnc: function(value) {
          return foramttedValue(value);
        }
      },
      chartPadding: {
        right: 50
      }
    }

    return (
      <ChartistGraph data={data} options={options} type='Line' />
    )
  }
})