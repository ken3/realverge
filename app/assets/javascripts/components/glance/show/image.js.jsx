/* global _ React $ GlanceImage */
var GlanceImage = React.createClass({

  propTypes: {
    data: React.PropTypes.object
  },

  render: function() {
    var src = this.props.data && this.props.data.src
    return (
      <div className='card-image'>
        <img src={src} />
      </div>
    )
  }
})