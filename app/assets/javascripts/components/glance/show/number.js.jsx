/* global _ React $ GlanceNumber foramttedValue */
var GlanceNumber = React.createClass({

  propTypes: {
    data: React.PropTypes.object
  },

  render: function() {
    var val = this.props.data && this.props.data.value
    return (
      <div className='fittext' style={{ height: '100%' }}>
        <div className='valign-wrapper' style={{ height: '80%', width: '100%' }}>
          <div className='valign center-align' style={{ width: '100%' }}>
            {foramttedValue(val)}
          </div>
        </div>
      </div>
    )
  }
})