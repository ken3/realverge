/* global _ React $ GlanceNumber */
var GlanceTitle = React.createClass({

  propTypes: {
    data: React.PropTypes.object
  },

  render: function() {
    var value = this.props.data && this.props.data.value
    return (
      <div className='fittext' style={{ height: '100%' }}>
        <div className='valign-wrapper' style={{ height: '80%', width: '100%' }}>
          <div className='valign center-align' style={{ width: '100%' }}>
            {value}
          </div>
        </div>
      </div>
    )
  }
})