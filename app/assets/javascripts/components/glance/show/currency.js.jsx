/* global _ React $ GlanceNumber numeral currencies */
var GlanceCurrency = React.createClass({

  propTypes: {
    data: React.PropTypes.object
  },

  render: function() {
    var value   = this.props.data && this.props.data.value
    var isoCode = (this.props.data && this.props.data.iso_code) || 'USD'
    var n = currencies.setLocal(isoCode)
    return (
      <div className='fittext' style={{ height: '100%' }}>
        <div className='valign-wrapper' style={{ height: '80%', width: '100%' }}>
          <div className='valign center-align' style={{ width: '100%' }}>
            {n(value).format('($0.00 a)')}
          </div>
        </div>
      </div>
    )
  }
})