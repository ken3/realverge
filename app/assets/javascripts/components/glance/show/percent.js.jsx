/* global _ React $ GlanceNumber numeral */
var GlancePercent = React.createClass({

  propTypes: {
    data: React.PropTypes.object
  },

  render: function() {
    return (
      <div className='fittext' style={{ height: '100%' }}>
        <div className='valign-wrapper' style={{ height: '80%', width: '100%' }}>
          <div className='valign center-align' style={{ width: '100%' }}>
            {this.props.data.value}
          </div>
        </div>
      </div>
    )
  }
})