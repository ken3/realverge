/* global _ React $ ReactDOM Native Materialize currencies */
var Native = React.createClass({

  propTypes: {
    glance: React.PropTypes.object,
    updateForm: React.PropTypes.func,
    editing: React.PropTypes.bool
  },

  componentDidMount: function () {
    var ouput = this.getOutput()
    this.props.updateForm({ output: ouput })
  },

  componentWillReceiveProps: function(newProps) {
    // Store output in glance's data object
    var ouput = this.getOutput()
    if(newProps.glance.data.output !== ouput) {
      this.props.updateForm({ output: ouput })
    }
  },

  // Number
  numberInput: function() {
    var glance = this.props.glance
    var id = this.props.editing ?
      'glance-native-number-new' :
      'glance-' + glance.id
    return (
      <div className="input-field col s12">
        <label htmlFor={id}>Value</label>
        <input type='number' id={id} onChange={this.handleChange} value={glance.data.value} />
      </div>
    )
  },

  // Currency
  currencyInput: function() {
    var glance = this.props.glance
    var id = this.props.editing ?
      'glance-native-currency-value-new' :
      'glance-value-' + glance.id
    return (
      <div className='row'>
        <div className="input-field col s8">
          <label htmlFor={id}>Value</label>
          <input type='number' id={id} step="0.01" onChange={this.handleChange} value={glance.data.value} />
        </div>
        <div className="input-field col s4">
          <select className="browser-default" value={glance.data.iso_code} onChange={this.handleCurrencyChange}>
            {_.map(currencies.locals, function(value, index) {
              return (<option key={index} value={value['code']}>{value['name']}</option>)
            })}
          </select>
        </div>
      </div>
    )
  },

  // Title
  titleInput: function () {
    return <div />
  },

  // List
  listInput: function() {
    var data   = this.props.glance.data
    var length = data.series && data.series.length || 1
    var that   = this
    return (
      <div className='row'>
        {_.map(_.slice(data.series, 0, length - 1), function(value, index) {
          return (
            <div key={index}>
              <div className='col s11'>
                <div className='input-field'>
                  <label htmlFor={'glance-native-list-' + index}>Value</label>
                  <input id={'glance-native-list-' + index} onChange={function(event) { that.handleListValueChange(event, index) }} type='text' value={value} />
                </div>
              </div>
              <div className='col s1'>
                <a href='#' onClick={function(event) { that.handleListRemove(event, index) }}>
                  <i className='material-icons red-text' style={{ lineHeight: '70px' }}>remove</i>
                </a>
              </div>
            </div>
          )
        })}
        <div className='col s11'>
          <div className='input-field'>
            <label htmlFor='glance-native-list-new'>Value</label>
            <input id='glance-native-list-new' type='text' ref='glanceNativeListNew' value={data.series && data.series[length - 1]} onChange={function(event) { that.handleListValueChange(event, length - 1) }} />
          </div>
        </div>
        <div className='col s1'>
          <a href='#' onClick={this.handleListAdd}>
            <i className='material-icons green-text' style={{ lineHeight: '70px' }}>add</i>
          </a>
        </div>
      </div>
    )
  },

  // Line
  lineInput: function() {
    var data   = this.props.glance.data
    var that   = this
    var length = data.series ?
      Math.max(data.labels && data.labels.length, data.series && data.series.length) :
      1
    return (
      <div>
        {_.map(_.slice(data.series, 0, length - 1), function(value, index) {
          return (
            <div key={index}>
              <div className='col s5'>
                <div className='input-field'>
                  <label htmlFor={'glance-native-line-label-' + index}>Label</label>
                  <input id={'glance-native-line-label-' + index} onChange={function(event) { that.handleListLabelChange(event, index, 'labels') }} type='text' value={data.labels[index]} />
                </div>
              </div>
              <div className='col s5'>
                <div className='input-field'>
                  <label htmlFor={'glance-native-line-' + index}>Value</label>
                  <input id={'glance-native-line-' + index} onChange={function(event) { that.handleListValueChange(event, index) }} type='number' value={value} />
                </div>
              </div>
              <div className='col s1'>
                <a href='#' onClick={function(event) { that.handleLineRemove(event, index) }}>
                  <i className='material-icons red-text' style={{ lineHeight: '70px' }}>remove</i>
                </a>
              </div>
            </div>
          )
        })}
        <div className='col s5'>
          <div className='input-field'>
            <label htmlFor='glance-native-line-new-label'>Label</label>
            <input id='glance-native-line-new-label' type='text' ref='glanceNativeLineNewLabel' value={data.labels && data.labels[length - 1]} onChange={function(event) { that.handleListLabelChange(event, length - 1, 'labels') }} />
          </div>
        </div>
        <div className='col s5'>
          <div className='input-field'>
            <label htmlFor='glance-native-line-new'>Value</label>
            <input id='glance-native-line-new' type='number' ref='glanceNativeLineNew' value={data.series && data.series[length - 1]} onChange={function(event) { that.handleListValueChange(event, length - 1) }} />
          </div>
        </div>
        <div className='col s1'>
          <a href='#' onClick={this.handleLineAdd}>
            <i className='material-icons green-text' style={{ lineHeight: '70px' }}>add</i>
          </a>
        </div>
      </div>
    )
  },

  // Paragraph
  paragraphInput: function() {
    var glance = this.props.glance
    var id = this.props.editing ?
      'glance-native-paragraph-new' :
      'glance-' + glance.id
    return (
      <div className="input-field col s12">
        <label htmlFor={id}>Value</label>
        <textarea id={id} onChange={this.handleChange} value={glance.data.value} className='materialize-textarea' />
      </div>
    )
  },

  // Image
  imageInput: function() {
    var glance = this.props.glance
    var id = this.props.editing ?
      'glance-native-image-new' :
      'glance-' + glance.id
    return (
      <div className="input-field col s12">
        <label htmlFor={id}>URL</label>
        <input type='text' id={id} onChange={this.handleImageChange} value={glance.data.src} className='materialize-textarea' />
      </div>
    )
  },

  handleChange: function(event) {
    var value = event.target.value
    var iso_code = this.props.glance && this.props.glance.data && this.props.glance.data.iso_code || 'USD'
    this.props.updateForm({ value: value, iso_code: iso_code })
  },

  handleImageChange: function(event) {
    var value = event.target.value
    this.props.updateForm({ src: value })
  },

  handleCurrencyChange: function (event) {
    var value = event.target.value
    this.props.updateForm({ iso_code: value })
  },

  handleListValueChange: function(event, index) {
    var value = event.target.value
    var series = this.props.glance.data.series || []
    series[index] = value
    this.props.updateForm({
      series: series
    })
  },

  handleListLabelChange: function(event, index) {
    var value = event.target.value
    var labels = this.props.glance.data.labels || []
    labels[index] = value
    this.props.updateForm({
      labels: labels
    })
  },

  handleListAdd: function(event) {
    event.preventDefault()
    var series = this.props.glance.data.series || []
    this.refs.glanceNativeListNew.value = ''
    series.push('')
    this.props.updateForm({ series: series })
  },

  handleLineAdd: function(event) {
    event.preventDefault()
    var series = this.props.glance.data.series || []
    var labels = this.props.glance.data.labels || []
    this.refs.glanceNativeLineNew.value = ''
    this.refs.glanceNativeLineNewLabel.value = ''
    series.push('')
    labels.push('')
    this.props.updateForm({ series: series, labels: labels })
  },

  handleListRemove: function(event, index) {
    event.preventDefault()
    var series = this.props.glance.data.series
    this.props.updateForm({
      series: [..._.slice(series, 0, index), ..._.slice(series, index + 1)]
    })
  },

  handleLineRemove: function(_event, index) {
    _event.preventDefault()
    var series = this.props.glance.data.series
    var labels = this.props.glance.data.labels
    this.props.updateForm({
      series: [..._.slice(series, 0, index), ..._.slice(series, index + 1)],
      labels: [..._.slice(labels, 0, index), ..._.slice(labels, index + 1)]
    })
  },

  getOutput: function() {
    var kind = this.props.glance.kind && this.props.glance.kind.toLowerCase()
    kind = kind.split('_')
    kind = kind.length > 1 ? kind[1] : kind[0]
    return kind
  },

  render: function() {
    var glance      = this.props.glance
    var data        = glance.data
    var public_form = glance.public_form || false
    var kind        = glance.kind.toLowerCase()
    var form        = <div />
    var speakeasyId = !!glance.id ?
      glance.id + '-glance-public-form' :
      'new-glance-public-form'

    kind = _.last(kind.split('_'))
    switch(kind) {
      case 'number':
        form = this.numberInput()
        break;
      case 'image':
        form = this.imageInput()
        break;
      case 'currency':
        form = this.currencyInput()
        break;
      case 'list':
        form = this.listInput()
        break;
      case 'line':
      case 'pie':
      case 'bar':
      case 'donut':
        form = this.lineInput()
        break;
      case 'paragraph':
        form = this.paragraphInput()
        break;
      case 'title':
        form = this.titleInput()
        break;
      default:
        form = <div />
    }
    return (
      <div>
        {form}
        {(kind !== 'title') && !this.props.hideSpeakeasy &&
          <div className='input-field col s12'>
            <input type='hidden' name='graph[public_form]' value='off' />
            <input name='graph[public_form]' type='checkbox' id={speakeasyId} defaultChecked={public_form} />
            <label htmlFor={speakeasyId}>Enable SpeakeasyLink™?</label>
          </div>
        }
      </div>
    )
  }
})