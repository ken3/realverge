/* global _ React GlanceForm */
var GlanceForm = React.createClass({

  propTypes: {
    glance: React.PropTypes.object,
    connectionName: React.PropTypes.string,
    updateForm: React.PropTypes.func
  },

  render: function() {
    var cn = this.props.connectionName && this.props.connectionName.toLowerCase()
    cn = _.first(cn.split('_'))
    switch(cn) {
      case 'bombbomb':
        return <Bombbomb {...this.props} />
      case 'facebook':
        return <Facebook {...this.props} />
      case 'followupboss':
        return <Followupboss {...this.props} />
      case 'google':
        return <Google {...this.props} />
      case 'hubspot':
        return <Hubspot {...this.props} />
      case 'native':
        return <Native {...this.props} />
      case 'zapier':
        return <Zapier {...this.props} />
      default:
        return <div />
    }
  }
});
