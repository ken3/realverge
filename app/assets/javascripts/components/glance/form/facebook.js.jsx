/* global _ React $ ReactDOM Google */
var Facebook = React.createClass({

  propTypes: {
    glance: React.PropTypes.object,
    updateForm: React.PropTypes.func,
    editing: React.PropTypes.bool
  },

  getInitialState: function() {
    return {
      pages: [],
      title: 'Load Data',
      disable: false
    }
  },

  componentDidMount: function() {
    var id = this.props.glance.integration_id
    var editing = this.props.editing
    if(!editing) {
      this.getPages(id)
    }
  },

  getPages: function(id) {
    var that = this
    $.get('/connections/' + id,
      { f: 'get_pages' },
      function(data) {
        that.setState({ pages: data })
      }
    )
  },

  pagesSelect: function() {
    var page_id = this.props.glance.data && this.props.glance.data.page_id
    var pages = this.state.pages
    return(
      <select
        className='browser-default'
        value={page_id || -1}
        onChange={this.handlePageChange}
        disabled={!pages.length}
      >
        <option key={-1} value={-1} disabled={true}>Select Facebook Page</option>
        {_.map(pages, function(g, i) {
          return <option key={i} value={g['id']}>{g['name']}</option>
        })}
      </select>
    )
  },

  handlePageChange: function (event) {
    var page_id = event.target.value
    this.props.updateForm({ page_id: page_id, output: 'number' })
  },

  updateData: function (event) {
    event.preventDefault()
    var id   = this.props.glance.integration_id
    var that = this
    this.setState({ disable: true })
    $.get('/connections/' + id,
      { f: 'get_data', kind: that.props.glance.kind, ...that.props.glance.data },
      function(data) {
        that.setState({ title: 'Reload Data', disable: false }, function () {
          that.props.updateForm(data)
        })
      }
    )
  },

  showButton: function () {
    return this.props.glance.data && !!this.props.glance.data.page_id
  },

  render: function() {
    var editing = this.props.editing
    var title   = this.state.title
    var disable = this.state.disable
    return (
      <div>
        {!editing &&
          <div className='input-field'>
            {this.pagesSelect()}
          </div>
        }
        {this.showButton() &&
          <div className='row hide-on-small-only'>
            <br />
            <a href='#' onClick={this.updateData} className={'btn col s8 offset-s2 ' + (disable ? 'disabled' : '')}>
              {title}
            </a>
            {disable &&
              <div className='col s2'>
                <div className="preloader-wrapper small active">
                  <div className="spinner-layer">
                    <div className="circle-clipper left">
                      <div className="circle"></div>
                    </div><div className="gap-patch">
                      <div className="circle"></div>
                    </div><div className="circle-clipper right">
                      <div className="circle"></div>
                    </div>
                  </div>
                </div>
              </div>
            }
          </div>
        }
      </div>
    )
  }
});