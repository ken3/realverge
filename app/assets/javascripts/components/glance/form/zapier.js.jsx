/* global _ React $ ReactDOM Zapier ReactRailsUJS */
var Zapier = React.createClass({

  propTypes: {
    glance: React.PropTypes.object,
    updateForm: React.PropTypes.func,
    editing: React.PropTypes.bool
  },

  componentWillMount: function () {
    var glance = this.props.glance
    if(!this.props.editing) {
      this.props.updateForm({
        zapier_id: Date.now(),
        output: _.last(_.split(glance.kind, '_'))
      })
    }
  },

  path: function () {
    var id     = this.props.glance.data && this.props.glance.data.zapier_id
    var domain = 'https://' + document.domain
    return (domain + '/api/zapier/glances/' + id)
  },

  render: function() {
    return (
      <div className='row'>
        <div className='input-field col s12 m8'>
          <input type='text' className='copy-source' disabled={true} value={this.path()} />
        </div>
        <div className='input-field col s12 m4'>
          <a href='#' className='waves-effect waves-light btn copy-btn col s12'>
            <i className='material-icons left'>content_copy</i>
            Copy
          </a>
        </div>
        <div className='input-field col s12'>
          <blockquote className='flow-text'>Copy the above link into the Zapier's zap form.</blockquote>
        </div>
      </div>
    )
  }
})