/* global _ React $ ReactDOM Hubspot */
var Hubspot = React.createClass({

  propTypes: {
    glance: React.PropTypes.object,
    updateForm: React.PropTypes.func,
    editing: React.PropTypes.bool
  },

  getInitialState: function() {
    return {
      title: 'Load Data',
      disable: false
    }
  },

  updateData: function (event) {
    event.preventDefault()
    var id   = this.props.glance.integration_id
    var that = this
    this.setState({ disable: true })
    $.get('/connections/' + id,
      { f: 'get_data', kind: that.props.glance.kind, ...that.props.glance.data },
      function(data) {
        that.setState({ title: 'Reload Data', disable: false }, function () {
          that.props.updateForm(data)
        })
      }
    )
  },

  render: function() {
    var editing = this.props.editing
    var title   = this.state.title
    var disable = this.state.disable
    return (
      <div>
        <div className='row hide-on-small-only'>
          <br />
          <a href='#' onClick={this.updateData} className={'btn col s8 offset-s2 ' + (disable ? 'disabled' : '')}>
            {title}
          </a>
          {disable &&
            <div className='col s2'>
              <div className="preloader-wrapper small active">
                <div className="spinner-layer">
                  <div className="circle-clipper left">
                    <div className="circle"></div>
                  </div><div className="gap-patch">
                    <div className="circle"></div>
                  </div><div className="circle-clipper right">
                    <div className="circle"></div>
                  </div>
                </div>
              </div>
            </div>
          }
        </div>
      </div>
    )
  }
})
