/* global _ React $ ReactDOM Google initFitText initMarquee */
var Google = React.createClass({

  propTypes: {
    glance: React.PropTypes.object,
    updateForm: React.PropTypes.func,
    editing: React.PropTypes.bool
  },

  getInitialState: function() {
    return {
      spreadsheets: [],
      worksheets: [],
      title: 'Load Data',
      disable: false,
      characterRange: _.range(1, 27),
      numberRange: _.range(1, 101)
    }
  },

  componentDidMount: function() {
    var id      = this.props.glance.integration_id
    var editing = this.props.editing
    if(!editing) {
      this.getSpreadsheets(id)
    }
  },

  getSpreadsheets: function(id) {
    var that = this
    $.get('/connections/' + id,
      { f: 'get_spreadsheets' },
      function(data) {
        that.setState({ spreadsheets: data })
      }
    )
  },

  getWorksheets: function(id, spreadsheet_id) {
    var that = this
    $.get('/connections/' + id,
      {
        f: 'get_worksheets',
        spreadsheet_id: spreadsheet_id
      },
      function(data) {
        that.setState({ worksheets: data })
      }
    )
  },

  updateData: function (event) {
    event.preventDefault()
    var that          = this
    var glance        = this.props.glance
    var kind          = glance && glance.kind
    var output        = kind && _.last(_.split(kind, '_')).toLowerCase()
    var id            = glance.integration_id
    var range         = glance.data && glance.data.range
    var range_labels  = glance.data && glance.data.range_labels

    this.setState({ disable: true, shake: false })

    $.get('/connections/' + id,
      { f: 'get_data', kind: kind, ...that.props.glance.data, range: range, range_labels: range_labels },
      function(data) {
        that.props.updateForm({ output: output, ...data, data: { range: range, range_labels: range_labels } })
      }
    ).always(function() {
      that.setState({ title: 'Reload Data', disable: false })
    }).fail(function() {
      that.setState({ shake: true })
    })
  },

  spreadsheetSelect: function() {
    var sheet_id = this.props.glance.data && this.props.glance.data.sheet_id
    var spreadsheets = this.state.spreadsheets
    return(
      <select
        className='browser-default'
        value={sheet_id || -1}
        onChange={this.handleSpreadsheetChange}
        disabled={!spreadsheets.length}
      >
        <option key={-1} value={-1} disabled={true}>Select Spreadsheet</option>
        {_.map(spreadsheets, function(g, i) {
          return <option key={i} value={g['id']}>{g['name']}</option>
        })}
      </select>
    )
  },

  worksheetSelect: function() {
    var sheet_name = this.props.glance.data && this.props.glance.data.sheet_name
    var worksheets = this.state.worksheets
    return(
      <select
        className='browser-default'
        value={sheet_name || -1}
        onChange={this.handleWorksheetChange}
        disabled={!worksheets.length}
      >
        <option key={-1} value={-1} disabled={true}>Select Worksheets</option>
        {_.map(worksheets, function(w, i) {
          return <option key={i} value={w}>{w}</option>
        })}
      </select>
    )
  },

  handleSpreadsheetChange: function (event) {
    var sheet_id = event.target.value
    var integration_id  = this.props.glance.integration_id
    this.props.updateForm({ sheet_id: sheet_id })
    this.getWorksheets(integration_id, sheet_id)
  },

  handleWorksheetChange: function (event) {
    var sheet_name = event.target.value
    this.props.updateForm({ sheet_name: sheet_name })
  },

  handleValueRangeChange: function (event, spot) {
    var value  = event.target.value
    var range  = (this.props.glance.data && this.props.glance.data.range) || ''
    var length = range.length

    if(spot === 'col') {
      range = value + (range[length - 1] || '')
    } else {
      range = (range[0] || '') + value
    }

    range = range.replace('!', '')
    range = (range.length === 1) ? (range[0] + ':' + range[0]) : range

    this.props.updateForm({ range: range })
  },

  handleLabelRangeChange: function (event, spot) {
    var value  = event.target.value
    var range  = (this.props.glance.data && this.props.glance.data.range_labels) || ''
    var length = range.length

    if(spot === 'col') {
      range = value + (range[length - 1] || '')
    } else {
      range = (range[0] || '') + value
    }

    range = range.replace('!', '')
    range = (range.length === 1) ? (range[0] + ':' + range[0]) : range

    this.props.updateForm({ range_labels: range })
  },

  canSubmit: function () {
    return this.props.glance.data &&
      !!this.props.glance.data.sheet_id &&
      !!this.props.glance.data.sheet_name
  },

  singleInput: function(output) {
    return _.includes([
      "currency",
      "number",
      "list"
    ], output)
  },

  render: function() {
    var that           = this
    var data           = this.props.glance.data
    var kind           = this.props.glance.kind
    var output         = kind && _.last(_.split(kind, '_')).toLowerCase()
    var range          = data && data.range
    var rangeCol       = (data && data.range && _.first(range.split(''))) || '-1'
    var rangeRow       = (data && data.range && _.last(range.split(''))) || '-1'
    var rangeLabels    = data && data.range_labels
    var rangeLabelCol  = (rangeLabels && _.first(rangeLabels.split(''))) || '-1'
    var rangeLabelRow  = (rangeLabels && _.last(rangeLabels.split(''))) || '-1'
    var editing        = this.props.editing
    var title          = this.state.title
    var disable        = this.state.disable
    var shake          = this.state.shake
    var characterRange = this.state.characterRange
    var numberRange    = this.state.numberRange

    if(range && _.includes(range.split(''), ':')) {
      if(typeof range[0] === 'number') {
        rangeCol= '!'
      } else {
        rangeRow = '!'
      }
    }

    if(rangeLabels && _.includes(rangeLabels.split(''), ':')) {
      if(typeof rangeLabels[0] === 'number') {
        rangeLabelCol= '!'
      } else {
        rangeLabelRow = '!'
      }
    }

    return (
      <div>
        {!editing &&
          <div>
            <div className='input-field'>
              {this.spreadsheetSelect()}
            </div>
            <div className='input-field'>
              {this.worksheetSelect()}
            </div>
          </div>
        }
        {this.canSubmit() &&
          <div className='row'>
            <div className='input-field'>
              <div className='col s3' style={{ lineHeight: '40px' }}>{this.singleInput(output) ? 'Value' : 'Values' }</div>
              <select className='col s4 browser-default' value={rangeCol} onChange={function(event) { that.handleValueRangeChange(event, 'col') }}>
                <option key={-1} value='-1' disabled={true}>Column</option>
                <option key={0} value='!'>Entire Row</option>
                {_.map(characterRange, function (i) {
                  var character = String.fromCharCode(i + 64)
                  return <option key={i} value={character}>{character}</option>
                })}
              </select>
              <select className='col s4 offset-s1 browser-default' value={rangeRow} onChange={function(event) { that.handleValueRangeChange(event, 'row') }}>
                <option key={-1} value='-1' disabled={true}>Row</option>
                <option key={0} value='!'>Entire Column</option>
                {_.map(numberRange, function (i) {
                  return <option key={i} value={i}>{i}</option>
                })}
              </select>
            </div>
          </div>
        }
        {!this.singleInput(output) && this.canSubmit() &&
          <div className='row'>
            <div className='input-field'>
              <div className='col s3' style={{ lineHeight: '40px' }}>Labels</div>
              <select className='col s4 browser-default' value={rangeLabelCol} onChange={function(event) { that.handleLabelRangeChange(event, 'col') }}>
                <option key={-1} value='-1' disabled={true}>Column</option>
                <option key={0} value='!'>Entire Row</option>
                {_.map(characterRange, function (i) {
                  var character = String.fromCharCode(i + 64)
                  return <option key={i} value={character}>{character}</option>
                })}
              </select>
              <select className='col s4 offset-s1 browser-default' value={rangeLabelRow} onChange={function(event) { that.handleLabelRangeChange(event, 'row') }}>
                <option key={-1} value='-1' disabled={true}>Row</option>
                <option key={0} value='!'>Entire Column</option>
                {_.map(numberRange, function (i) {
                  return <option key={i} value={i}>{i}</option>
                })}
              </select>
            </div>
          </div>
        }
        {this.canSubmit() &&
          <div>
            <div className='row hide-on-small-only'>
              <a href='#' onClick={this.updateData} className={'btn col s8 offset-s2 ' + (disable ? 'disabled ' : ' ') + (shake ? 'shake' : '')}>
                {title}
              </a>
              {disable &&
                <div className='col s2'>
                  <div className="preloader-wrapper small active">
                    <div className="spinner-layer">
                      <div className="circle-clipper left">
                        <div className="circle"></div>
                      </div><div className="gap-patch">
                        <div className="circle"></div>
                      </div><div className="circle-clipper right">
                        <div className="circle"></div>
                      </div>
                    </div>
                  </div>
                </div>
              }
            </div>
            <div className='row'>
              <blockquote className='col s12'>
                When selecting entire column or row, the first column's row or the first row's column will be skipped so they can be labels within sheet.
              </blockquote>
            </div>
          </div>
        }
      </div>
    )
  }
});
