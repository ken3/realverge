/* global _ React $ ReactDOM Google */
var Bombbomb = React.createClass({

  propTypes: {
    glance: React.PropTypes.object,
    updateForm: React.PropTypes.func,
    editing: React.PropTypes.bool
  },

  getInitialState: function() {
    return {
      emails: [],
      lists: [],
      title: 'Load Data',
      disable: false
    }
  },

  componentDidMount: function() {
    var id = this.props.glance.integration_id
    var filter = this.filter()
    var editing = this.props.editing
    if(!editing) {
      filter == 'email' ?
        this.getEmails(id) :
        this.getLists(id)
    }
  },

  getEmails: function(id) {
    var that = this
    $.get('/connections/' + id,
      { f: 'get_emails' },
      function(data) {
        that.setState({ emails: data })
      }
    )
  },

  getLists: function(id) {
    var that = this
    $.get('/connections/' + id,
      { f: 'get_lists' },
      function(data) {
        that.setState({ lists: data })
      }
    )
  },

  emailsSelect: function() {
    var email_id = this.props.glance.data && this.props.glance.data.email_id
    var emails = this.state.emails
    return(
      <select
        className='browser-default'
        value={email_id || -1}
        onChange={this.handleEmailChange}
        disabled={!emails.length}
      >
        <option key={-1} value={-1} disabled={true}>Select Email</option>
        {_.map(emails, function(g, i) {
          return <option key={i} value={g['id']}>{g['name']}</option>
        })}
      </select>
    )
  },

  listsSelect: function() {
    var list_id = this.props.glance.data && this.props.glance.data.list_id
    var lists = this.state.lists
    return(
      <select
        className='browser-default'
        value={list_id || -1}
        onChange={this.handleListChange}
        disabled={!lists.length}
      >
        <option key={-1} value={-1} disabled={true}>Select List</option>
        {_.map(lists, function(g, i) {
          return <option key={i} value={g['id']}>{g['name']}</option>
        })}
      </select>
    )
  },

  handleEmailChange: function (event) {
    var email_id = event.target.value
    this.props.updateForm({ email_id: email_id })
  },

  handleListChange: function (event) {
    var list_id = event.target.value
    this.props.updateForm({ list_id: list_id })
  },

  updateData: function (event) {
    event.preventDefault()
    var id   = this.props.glance.integration_id
    var that = this
    this.setState({ disable: true })
    $.get('/connections/' + id,
      { f: 'get_data', kind: that.props.glance.kind, ...that.props.glance.data },
      function(data) {
        that.setState({ title: 'Reload Data', disable: false }, function () {
          that.props.updateForm(data)
        })
      }
    )
  },

  showButton: function () {
    return (this.props.glance.data && !!this.props.glance.data.email_id) ||
           (this.props.glance.data && !!this.props.glance.data.list_id)  ||
           (this.pieChart())
  },

  filter: function () {
    return _.includes(this.props.glance.kind, 'email') ? 'email' : 'list'
  },

  pieChart: function () {
    return _.includes(this.props.glance.kind, 'pie')
  },

  render: function() {
    var editing = this.props.editing
    var title   = this.state.title
    var disable = this.state.disable
    return (
      <div>
        {!editing && !this.pieChart() &&
          <div className='input-field'>
            {this.filter() === 'email' ? this.emailsSelect() : this.listsSelect() }
          </div>
        }
        {this.showButton() &&
          <div className='row hide-on-small-only'>
            <br />
            <a href='#' onClick={this.updateData} className={'btn col s8 offset-s2 ' + (disable ? 'disabled' : '')}>
              {title}
            </a>
            {disable &&
              <div className='col s2'>
                <div className="preloader-wrapper small active">
                  <div className="spinner-layer">
                    <div className="circle-clipper left">
                      <div className="circle"></div>
                    </div><div className="gap-patch">
                      <div className="circle"></div>
                    </div><div className="circle-clipper right">
                      <div className="circle"></div>
                    </div>
                  </div>
                </div>
              </div>
            }
          </div>
        }
      </div>
    )
  }
});