/* global _ React $ GlanceEdit hideSpeakeasy GlanceForm */
var GlanceEdit = React.createClass({

  propTypes: {
    glance: React.PropTypes.object,
    hideSpeakeasy: React.PropTypes.bool
  },

  getInitialState: function() {
    return {
      glance: this.props.glance
    }
  },

  updateForm: function(newData) {
    var glance = this.state.glance
    this.setState({
      glance: { ...glance, data: { ...this.state.glance.data, ...newData } }
    }, function () {
      $('#edit-glance-' + glance.id).val(JSON.stringify(this.state.glance.data))
      if(window.location.hostname === 'realverge-kenanstipek.c9users.io') {
        console.log('Form State: ', this.state.glance)
      }
    })
  },

  render: function() {
    var glance = this.state.glance
    var cn = glance.kind.split('_')[0]
    return (
      <div>
        <div className='col m6 s12 no-gutter'>
          <GlanceForm
            updateForm={this.updateForm}
            connectionName={cn}
            glance={glance}
            hideSpeakeasy={this.props.hideSpeakeasy}
            editing
          />
        </div>
        <div className='col m6 hide-on-small-only'>
          <GlanceShow
            forceHeight='300px'
            glances={
              [{
                ...glance,
                title: 'Glance Preview',
                size: 12
              }]}
          />
        </div>
      </div>
    );
  }
});