/* global _ React $ GlanceShow initFitText initMarquee */
var GlanceShow = React.createClass({

  propTypes: {
    glances: React.PropTypes.array
  },

  componentDidUpdate: function () {
    if(_.reduce(this.props.glances, function(a, i) { return !!a.data && !!i.data })) {
      initFitText()
      initMarquee()
    }
  },

  defaultOutput: function (glance, forceHeight) {
    return (
      <div>
        No Data
      </div>
    )
  },

  render: function() {
    var that        = this
    var glances     = this.props.glances
    var forceHeight = !!this.props.forceHeight ? this.props.forceHeight : undefined
    return (
      <div>
        {_.map(glances, function(glance, i) {
          switch(glance.data && glance.data.output) {
            case 'number':
              return (
                <Card key={i} size={glance.size} forceHeight={forceHeight} provider={_.split(glance.kind, '_')[0]}>
                  <div className='card-title truncate'>{glance.title}</div>
                  {(!!glance.data.value || glance.data.value === 0) ?
                    <GlanceNumber key={glance.id} {...glance} /> :
                    that.defaultOutput(glance, forceHeight)
                  }
                </Card>
              )
            case 'percent':
              return (
                <Card key={i} size={glance.size} forceHeight={forceHeight} provider={_.split(glance.kind, '_')[0]}>
                  <div className='card-title truncate'>{glance.title}</div>
                  {(!!glance.data.value || glance.data.value === 0) ?
                    <GlancePercent key={glance.id} {...glance} /> :
                    that.defaultOutput(glance, forceHeight)
                  }
                </Card>
              )
            case 'title':
              return (
                <Card key={i} size={glance.size} forceHeight={forceHeight}>
                  <br />
                  <GlanceTitle key={glance.id} {...glance} data={{ value: $('#graph_title').val(), ...glance.data }} />
                </Card>
              )
            case 'currency':
              return (
                <Card key={i} size={glance.size} forceHeight={forceHeight} provider={_.split(glance.kind, '_')[0]}>
                  <div className='card-title truncate'>{glance.title}</div>
                  {!!glance.data.value ?
                    <GlanceCurrency key={glance.id} {...glance} /> :
                    that.defaultOutput(glance, forceHeight)
                  }
                </Card>
              )
            case 'line':
              return (
                <Card key={i} size={glance.size} forceHeight={forceHeight} provider={_.split(glance.kind, '_')[0]}>
                  <div className='card-title truncate'>{glance.title}</div>
                  <GlanceLine key={glance.id} {...glance} />
                </Card>
              )
            case 'bar':
              return (
                <Card key={i} size={glance.size} forceHeight={forceHeight} provider={_.split(glance.kind, '_')[0]}>
                  <div className='card-title truncate'>{glance.title}</div>
                  <GlanceBar key={glance.id} {...glance} />
                </Card>
              )
            case 'donut':
              return (
                <Card key={i} size={glance.size} forceHeight={forceHeight} provider={_.split(glance.kind, '_')[0]}>
                  <div className='card-title truncate'>{glance.title}</div>
                  <GlanceDonut key={glance.id} {...glance} />
                </Card>
              )
            case 'pie':
              return (
                <Card key={i} size={glance.size} forceHeight={forceHeight} provider={_.split(glance.kind, '_')[0]}>
                  <div className='card-title truncate'>{glance.title}</div>
                  <GlancePie key={glance.id} {...glance} />
                </Card>
              )
            case 'image':
              return (
                <Card key={i} noCardContent size={glance.size} forceHeight={forceHeight}>
                  {!!glance.data.src ?
                    <GlanceImage key={glance.id} {...glance} /> :
                    that.defaultOutput(glance, forceHeight)
                  }
                </Card>
              )
            case 'list':
              return (
                <Card key={i} size={glance.size} forceHeight={forceHeight} provider={_.split(glance.kind, '_')[0]}>
                  <div className='card-title truncate'>{glance.title}</div>
                  <GlanceList key={glance.id} {...glance} />
                </Card>
              )
            case 'paragraph':
              return (
                <Card key={i} size={glance.size} forceHeight={forceHeight} provider={_.split(glance.kind, '_')[0]}>
                  <GlanceParagraph key={glance.id} {...glance} />
                </Card>
              )
            default:
              return (
                <Card key={-1} size={glance.size} forceHeight={forceHeight}>
                  <div className='card-title truncate'>{glance.title}</div>
                  {that.defaultOutput(glance, forceHeight)}
                </Card>
              )
          }
        })}
      </div>
    )
  }
})