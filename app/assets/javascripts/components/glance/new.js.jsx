/* global _ React $ GlanceNew GlanceForm */
var GlanceNew = React.createClass({

  propTypes: {
    integrations: React.PropTypes.object,
    kinds: React.PropTypes.object
  },

  getInitialState: function() {
    return {
      glance: this.newGlance(),
      glances: {}
    }
  },

  newGlance: function() {
    return {
      integration_id: -1,
      kind: -1,
      data: {}
    }
  },

  updateForm: function(newData) {
    var glance = this.state.glance
    this.setState({
      glance: { ...glance, data: { ...this.state.glance.data, ...newData } }
    }, function () {
      $('#new-glance').val(JSON.stringify(this.state.glance.data))
      if(window.location.hostname === 'realverge-kenanstipek.c9users.io') {
        console.log('Form State: ', this.state.glance)
      }
    })
  },

  connectionSelect: function(integrations) {
    var integration_id = this.state.glance.integration_id
    return (
      <select
        className='browser-default'
        value={integration_id}
        onChange={this.handleConnectionChange}
      >
        <option key={-1} value={-1} disabled={true}>Select Connection</option>
        <option key={0} value='native'>Native</option>
        {_.map(integrations, function(v, k) {
          return <option key={k} value={k}>{_.capitalize(v)}</option>
        })}
      </select>
    )
  },

  glanceSelect: function(glances) {
    var kind = this.state.glance.kind
    return(
      <select
        className='browser-default'
        value={kind}
        onChange={this.handleGlanceChange}
      >
        <option key={-1} value={-1} disabled={true}>Select Glance</option>
        {_.map(glances, function(g, i) {
          return <option key={i} value={g}>{_.capitalize(g.replace(/-/g, ' '))}</option>
        })}
      </select>
    )
  },

  handleConnectionChange: function(event) {
    var integration_id = event.target.value
    var kinds          = this.props.kinds
    var integrations   = this.props.integrations

    console.log(integrations)

    // Reset glance before applying new connection type.
    this.setState({
      glance: this.newGlance()
    }, function() {
      this.setState({
        glance: _.merge(this.state.glance, { integration_id: integration_id }),
        glances: ((integration_id === 'native') ?
          kinds[integration_id]:
          kinds[integrations[integration_id]])
      }, function () {
        $('#new-glance-integration').val(integration_id)
      })
    })
  },

  handleGlanceChange: function(event) {
    var kind = event.target.value
    var glance = _.merge(this.state.glance, { kind: kind })
    glance.data = {}
    this.setState({
      glance: glance
    }, function() {
      $('#new-glance-kind').val((this.connectionName() + '_' + kind).toLowerCase())
    })
  },

  connectionName: function() {
    return this.state.glance.integration_id === 'native' ?
            'native' :
            this.props.integrations[this.state.glance.integration_id]
  },

  showGlances: function() {
    return this.state.glance.integration_id !== -1
  },

  showForm: function() {
    return this.showGlances() && (this.state.glance.kind !== -1)
  },

  render: function() {
    var integrations   = this.props.integrations
    var glances        = this.state.glances
    var glance         = this.state.glance
    var connectionName = this.connectionName()
    return (
      <div>
        <div className='col m6 s12'>
          <div className='input-field'>
            {this.connectionSelect(integrations)}
          </div>
          {this.showGlances() &&
            <div className='input-field'>
              {this.glanceSelect(glances)}
            </div>
          }
          {this.showForm() &&
            <GlanceForm
              updateForm={this.updateForm}
              connectionName={connectionName}
              glance={glance}
            />
          }
        </div>
        <div className='col m6 hide-on-small-only'>
          <GlanceShow
            forceHeight='300px'
            glances={
              [{
                ...glance,
                title: 'Glance Preview',
                size: 12,
                kind: ''
              }]}
          />
        </div>
      </div>
    );
  }
});