/* global _ React $ Card */
var Card = React.createClass({

  propTypes: {
    size: React.PropTypes.number,
    noCardContent: React.PropTypes.bool
  },

  height: function() {
    if(this.props.forceHeight !== undefined) {
      return this.props.forceHeight
    }
    var size = parseInt(this.props.size)
    switch(size) {
      case 2:
        return '24%'
      case 3:
        return '32%'
      case 6:
        return '49%'
      case 9:
        return '64%'
      case 12:
        return '99%'
      default:
        return '24%'
    }
  },

  render: function() {
    var size = this.props.size
    return (
      <div className={'col s12 m' + size} style={{ height: this.height(), padding: '0.5%', position: 'relative' }}>
        <div className='card' style={{ height: '100%' }}>
          {this.props.noCardContent ?
            this.props.children :
            <div className='card-content'>
              {this.props.children}
            </div>
          }
          <Badge provider={this.props.provider} />
        </div>
      </div>
    )
  }
})