/* global _ React $ Badge */
var Badge = React.createClass({

  propTypes: {
    provider: React.PropTypes.string
  },

  render: function() {
    var provider = this.props.provider
    var src      = '/' + provider + '-badge.png'
    return (
      (!!provider) ?
        <img src={src} className='brand-badge' /> :
        <div />
    )
  }
})