@foramttedValue = (val) ->
  n = numeral.language('en')
  if val > 1000
    val = n(val).format('0.0a')
  else
    val = if val % 1 == 0 then n(val).format('0a') else n(val).format('0.00a')
  val

# Helper functions
String.prototype.cap = () ->
  return (this.charAt(0).toUpperCase() + this.slice(1)).split('_').join(' ')
