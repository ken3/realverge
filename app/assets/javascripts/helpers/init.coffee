# Materialize setup
@initMaterialize = () ->
  Materialize.updateTextFields()
  $('select').material_select()
  $('.dropdown-button').dropdown()
  $('.modal-trigger').leanModal({
    ready: () -> ReactRailsUJS.mountComponents()
    complete: () -> $('.errors').addClass('hide')
  })

@initFitText = () ->
  $('.fittext').fitText(0.7, { maxFontSize: '240px', minFontSize: '50px' })

# Sorting
@initSorting = () ->
  $('.simple-sort').sortable({
    handle: '.grip',
    axis: 'y',
    cursor: 'pointer'
  })
  $('.sortable').sortable({
    handle: '.grip',
    axis: 'y',
    cursor: 'pointer',
    stop: (event, ui) ->
      $.ajax
        url: "/dashboards/#{$('.dashboard-graphs').data('id')}",
        type: 'PUT',
        data:
          dashboard:
            positions: ($(graph).data('id') for graph in $('.dashboard-graphs li'))
    }
  )

# Validation error catcher and printer
@initValidation = () ->
  $(document).on 'ajax:success', (_e, jqxhr) ->
    if(jqxhr['speakeasy'] == true)
      $('.modal:visible').find('#speakeasy')?.val(jqxhr['link'])
    else
      # Special case Zapier for weird create first approach.
      if(!_.includes(jqxhr['kind'], 'zapier'))
        $('.modal').closeModal()
      else
        $('#new-glance').val(JSON.stringify({ path: "/glance/#{jqxhr['id']}" }))
        ReactRailsUJS.mountComponents()
      $('.errors').addClass('hide')

  $(document).ajaxError (_e, jqxhr, _s, thrownError) ->
    if thrownError == "Unprocessable Entity"
      errors = $('<div>')
      for error of jqxhr.responseJSON
        errors.append("<div><strong>#{error.cap()}</strong></div>")
        errors.append("<div>#{message.cap()}</div>") for message in jqxhr.responseJSON[error]
      $('.errors').html(errors).removeClass('hide')

@initMarquee = () ->
  $('.marquee-container').each((_, mq) ->
    mq = $(mq)
    height = $('ul', mq).height()
    innerHeight = mq.parent().height()
    usedHeight = (if((height / 2) > innerHeight) then (height / 2) else innerHeight)
    if(height > innerHeight)
      mq.height(usedHeight)
      mq.width(mq.parent().width())
      $('.marquee-placeholder', mq).css(height: usedHeight)
      $('.marquee-placeholder', mq).addClass('marquee')
  )

@initDateLabels = () ->
  $('.date-label').each((_, e) ->
    unix = $(e).data('time')
    $(e).text(moment.unix(unix).utcOffset(TZ).format('MMM Do'))
  )

@initStripe = () ->
  $ ->
    $form = $('#payment-form')
    $form.submit (event) ->
      $form.find('.submit').prop 'disabled', true
      Stripe.card.createToken $form, stripeResponseHandler
      false
    return

  stripeResponseHandler = (status, response) ->
    $form = $('#payment-form')
    if response.error
      $form.find('.payment-errors > div').text response.error.message
      $form.find('.payment-errors').removeClass('hide')
      $form.find('.submit').prop 'disabled', false
    else
      token        = response.id
      cc_last_four = response.card.last4
      console.log(response)
      $form.find('.payment-errors').addClass('hide')
      $form.append $('<input type="hidden" name="stripe_token">').val(token)
      $form.append $('<input type="hidden" name="cc_last_four">').val(cc_last_four)
      $form.get(0).submit()
    return

loopValues = () ->
  controls: false,
  center: false,
  loop: true,
  help: true,
  history: true,
  overview: true,
  autoSlideStoppable: true,
  transitionSpeed: 'slow',
  autoSlide: $('#loop').attr('data-duration') * 1000,
  transition: $('#loop').attr('data-transition'),
  width: "100%",
  height: "100%",
  margin: 0,
  minScale: 1,
  maxScale: 1

if !!$('.reveal').length
  Reveal.addEventListener 'slidechanged', (event) ->
    $('.ct-chart').hide()
    window.setTimeout(() =>
      $('.ct-chart').fadeIn(500)
      ReactRailsUJS.mountComponents()
    , 1301)

@reinitLoop = (callback) ->
  Reveal.configure(do -> loopValues()) unless !$('.reveal').length
  callback()

@initLoop = (callback) ->
  Reveal.initialize(do -> loopValues()) unless !$('.reveal').length
  callback()

@openWaitingModal = () ->
  $(watingModalId).openModal() if watingModalId?