@update_ratios = () ->
  width = $(window).width()
  ratio = $(window).height() / width
  for size in [2, 3, 4, 6, 8, 9, 10, 12]
    do -> $(".graph.col.m#{size} .card").height(get_height(size, width, ratio))
  $('.graph.col.s12 .card').height(get_height(3, width, ratio)) if width < 600

@get_height = (size, width, ratio) ->
  switch size
    when 2  then (width / 6) * ratio - 5 # Tiny
    when 3  then (width / 4) * ratio     # Extra Small
    when 4  then (width / 3) * ratio     # Small
    when 6  then (width / 2) * ratio + 4 # Medium
    when 8  then (width * (2/3)) * ratio + 9 # Large
    when 9  then (width * (3/4)) * ratio + 19 # Extra Large
    when 10 then (width * (4/5)) * ratio + 59 # Huge
    when 12 then (width * ratio)