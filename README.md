# Realverge
### Visual KPI for Real Estate

## Purpose
To bring Real Estate Agent's key performance indicators to one place.

## Features
* Graph Pages, public and private
* Customizable graphs
* Third party real estate web tools integration
* Single account, login and signup with Facebook and Google

## Graph Types
* Number
* Bar Chart
* Funnel
* Leaderboard
* Line Chart
* List
* Text
* Pie Chart
* Image

## Integrating Products
* Zillow