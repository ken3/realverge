ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  module SignInHelper
    def sign_in_as(account)
      post login_path(email: account.email, password: account.password)
    end
  end

  class ActionDispatch::IntegrationTest
    include SignInHelper
  end
end
