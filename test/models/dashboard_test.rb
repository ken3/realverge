require 'test_helper'
require 'sidekiq/testing'
Sidekiq::Testing.fake!

class DashboardTest < ActiveSupport::TestCase
  setup do
    @dashboard = Fabricate(:dashboard)
  end

  test "duplicate board" do
    @dashboard.graphs << Fabricate(:graph)
    new_board = @dashboard.duplicate!
    assert_equal @dashboard.graphs.count, new_board.graphs.count
    assert_equal @dashboard.graphs.count, new_board.graphs.count
    assert_equal @dashboard.name, new_board.name
  end

  test "duplicate board with supplied name" do
    name = 'new name'
    new_board = @dashboard.duplicate!(name)
    assert_equal name, new_board.name
    assert_not_equal name, @dashboard.name
  end

  test "add title graph" do
    assert_equal @dashboard.graphs.count, 1
    assert_equal @dashboard.graphs.first.title, @dashboard.name
  end

  test "broadcast after touch" do
    DashboardBroadcast.jobs.clear
    assert_equal 0, DashboardBroadcast.jobs.size
    @dashboard.touch
    assert_equal 1, DashboardBroadcast.jobs.size
  end

  test "broadcast after update" do
    DashboardBroadcast.jobs.clear
    assert_equal 0, DashboardBroadcast.jobs.size
    @dashboard.update(name: 'test')
    assert_equal 1, DashboardBroadcast.jobs.size
  end

  test "check graph positions" do
    first_graph  = @dashboard.graphs.first
    second_graph = Fabricate(:graph, dashboard: @dashboard)
    assert_equal @dashboard.graphs_position_order.to_a.first, first_graph
    assert_equal @dashboard.graphs_position_order.to_a.last, second_graph
  end

  test "check graph positions after update" do
    first_graph  = @dashboard.graphs.first
    second_graph = Fabricate(:graph, dashboard: @dashboard)
    @dashboard.update(positions: [second_graph.id, first_graph.id])
    assert_equal @dashboard.graphs_position_order.to_a.last, first_graph
    assert_equal @dashboard.graphs_position_order.to_a.first, second_graph
  end
end
