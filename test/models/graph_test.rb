require 'test_helper'

class GraphTest < ActiveSupport::TestCase
  test "title copy on title kind creation" do
    title = 'test'
    graph = Fabricate(:graph, kind: 'native_title', title: title)
    assert_equal graph.data['value'], title
  end

  test "test size validation" do
    assert_raises(ActiveRecord::RecordInvalid) do
      Fabricate(:graph, kind: 'native_title', size: '0')
    end
  end

  test "number kind creation" do
    data = { value: 1 }
    graph = Fabricate(:graph, kind: 'native_number', data: data)
    assert_equal graph.data['value'], data[:value]
  end

  test "title kind creation" do
    data = { value: 'test' }
    graph = Fabricate(:graph, kind: 'native_title', title: data[:value])
    assert_equal graph.data['value'], data[:value]
  end

  test "currency kind creation" do
    data = {
      value: '1.25',
      iso_code: 'EUR'
    }
    graph = Fabricate(:graph, kind: 'native_currency', data: data)
    assert_equal graph.data['iso_code'], data[:iso_code]
    assert_equal graph.data['value'], data[:value].to_s
  end

  test "image kind creation" do
    data = { src: "https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97150&w=350&h=150" }
    graph = Fabricate(:graph, kind: 'native_image', data: data)
    assert_equal graph.data['src'], data[:src]
  end

  # Need to split up when logic deviates.
  test "line, bar, dials, contacts, and appointments kind creation" do
    data = {
      series: [1, 2, 3],
      labels: ['Ken', 'Brian', 'Avery']
    }
    graph = Fabricate(:graph, kind: 'native_line', data: data)
    assert_equal graph.data['series'], data[:series]
    assert_equal graph.data['labels'], data[:labels]
  end

  test "line, bar, dials, contacts, and appointments kind creation success with data gaps" do
    data = {
      series: [1, 2, '', 3],
      labels: ['Ken', 'Brian', 'John', 'Avery']
    }
    graph = Fabricate(:graph, kind: 'native_line', data: data)
    assert_equal graph.data['series'], [1, 2, '', 3]
    assert_equal graph.data['labels'], data[:labels]
  end

  test "donut, and pie kind creation success" do
    data = {
      series: [1, 2, 3],
      labels: ['Ken', 'Brian', 'Avery']
    }
    graph = Fabricate(:graph, kind: 'native_pie', data: data)
    assert_equal graph.data['series'], [1, 2, 3]
    assert_equal graph.data['labels'], data[:labels]
  end

  test "donut, and pie kind creation success with data gaps" do
    data = {
      series: [1, 2, '', 3],
      labels: ['Ken', 'Brian', 'John', 'Avery']
    }
    graph = Fabricate(:graph, kind: 'native_pie', data: data)
    assert_equal graph.data['series'], [1, 2, '', 3]
    assert_equal graph.data['labels'], data[:labels]
  end
end
