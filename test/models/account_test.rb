require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  setup do
    @account = Fabricate(:account)
  end

  test "firstname" do
    account = Fabricate(:account, name: 'Tom Selleck')
    assert_equal 'Tom', account.firstname
  end

  test "lastname" do
    account = Fabricate(:account, name: 'Tom Selleck')
    assert_equal 'Selleck', account.lastname
    account.name = 'Thomas William Selleck'
    assert_equal 'William Selleck', account.lastname
    account.name = 'Tom'
    assert_equal '', account.lastname
  end

  test "new account info" do
    assert_equal @account.dashboards.count, 1, 'Dashboards created'
    assert_equal @account.graphs.count, 2, 'Graphs created'
  end

  test "unique email" do
    assert_raises(ActiveRecord::RecordInvalid) do
      Fabricate(:account, email: @account.email)
    end
  end

  test "update last login" do
    time = @account.last_login_at
    @account.update_last_login_at
    assert_not_same time, @account.last_login_at
  end
end