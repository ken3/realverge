Fabricator(:account) do
  name      { Faker::Name.name }
  email     { Faker::Internet.email }
  password  { Faker::Internet.password }
  phone     { Faker::PhoneNumber.phone_number }
end