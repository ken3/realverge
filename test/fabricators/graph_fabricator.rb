Fabricator(:graph) do
  title { Faker::Company.name }
  size  { 2 }
  kind  { 'number' }
  data  { { value: 1} }
  dashboard
end