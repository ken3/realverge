require 'test_helper'

class DashboardsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @account   = Fabricate(:account)
    @dashboard = Fabricate(:dashboard, account: @account)
    sign_in_as(@account)
  end

  test "should get index" do
    get dashboards_path
    assert_response :success
    get root_url
    assert_response :success
  end

  test "should create dashboard" do
    assert_difference('Dashboard.count') do
      post dashboards_path, params: { dashboard: { name: 'name' } }
    end
    assert_redirected_to edit_dashboard_path(Dashboard.last)
  end

  test "should show dashboard" do
    get dashboard_path(@dashboard)
    assert_redirected_to public_board_path(@dashboard.uuid)
  end

  test "should get edit" do
    get edit_dashboard_path(@dashboard)
    assert_response :success
  end

  test "should update dashboard" do
    patch dashboard_path(@dashboard), params: { dashboard: { name: 'test' } }
    assert_redirected_to edit_dashboard_path(@dashboard)
  end

  test "should duplicate dashboard" do
    assert_difference('Dashboard.count', +1) do
      patch duplicate_dashboard_path(@dashboard), params: { dashboard: { name: 'test' } }
    end
  end

  test "should destroy dashboard" do
    assert_difference('Dashboard.count', -1) do
      delete dashboard_path(@dashboard)
    end
    assert_redirected_to root_url
  end
end
