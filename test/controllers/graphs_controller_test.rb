require 'test_helper'

class GraphsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @account   = Fabricate(:account)
    @dashboard = Fabricate(:dashboard, account: @account)
    @graph     = Fabricate(:graph, dashboard: @dashboard, public_form: true)
    sign_in_as(@account)
  end

  test "should create graph" do
    assert_difference('Graph.count') do
      post graphs_url, params: {
        graph: {
          kind: 'native_number',
          title: 'number',
          dashboard_id: @dashboard.id,
          data: '{"value":1}'
        }
      }
    end
    assert_redirected_to edit_dashboard_path(@dashboard)
  end

  test "should get edit" do
    get public_graph_form_url(uuid: @graph.uuid)
    assert_response :success
  end

  test "should update graph" do
    patch graph_url(@graph), params: { graph: { data: '{"value":2}' } }, env: { 'HTTP_REFERER': public_graph_form_url(uuid: @graph.uuid) }
    assert_redirected_to public_graph_form_url(uuid: @graph.uuid)
  end

  test "should reset graph uuid" do
    original_uuid = @graph.uuid
    patch reset_graph_speakeasy_path(@graph)
    graph = JSON.parse(@response.body)
    assert_not_equal original_uuid, graph['uuid']
  end

  test "should destroy graph" do
    assert_difference('Graph.count', -1) do
      delete graph_url(@graph)
    end
    assert_redirected_to edit_dashboard_path(@dashboard)
  end
end
