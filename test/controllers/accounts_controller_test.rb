require 'test_helper'

class AccountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @super   = Fabricate(:account, email: 'ken@agentecho.io')
    @account = Fabricate(:account)
  end

  test "should get index" do
    sign_in_as(@super)
    get accounts_path
    assert_response :success
  end

  test "should fail to access index" do
    sign_in_as(@account)
    get accounts_path
    assert_redirected_to logout_path
  end

  test "should get new" do
    get signup_path
    assert_response :success
  end

  test "should create account" do
    assert_difference('Account.count') do
      post accounts_path, params: {
        account: {
          name: Faker::Name.name,
          email: Faker::Internet.email,
          password: Faker::Internet.password
        }
      }
    end
    assert_redirected_to root_path(n: :t)
  end

  test "should get edit" do
    sign_in_as(@account)
    get settings_path(@account)
    assert_response :success
  end

  test "should update account" do
    sign_in_as(@account)
    patch account_path(@account), params: { account: { email: Faker::Internet.email } }, env: { 'HTTP_REFERER': account_path(@account) }
    assert_redirected_to account_path(@account)
  end

  test "should destroy account" do
    sign_in_as(@super)
    assert_difference('Account.count', -1) do
      delete account_path(@account)
    end
    assert_redirected_to accounts_path
  end
end
