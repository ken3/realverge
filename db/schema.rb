# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161116195420) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "phone"
    t.datetime "created_at",                                                           null: false
    t.datetime "updated_at",                                                           null: false
    t.string   "time_zone",                default: "Pacific Time (US & Canada)"
    t.string   "uid"
    t.string   "provider"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "deleted_at"
    t.datetime "last_login_at",            default: '2016-07-10 08:46:25'
    t.boolean  "email_optin",              default: true
    t.string   "source"
    t.datetime "last_seen_at",             default: '2016-07-24 21:11:55'
    t.integer  "referrer_id"
    t.integer  "referrals_count",          default: 0
    t.string   "zendesk_id"
    t.string   "hubspot_id"
    t.string   "password_reset_token"
    t.datetime "reset_password_date"
    t.string   "api_token"
    t.string   "persona"
    t.integer  "engagement_points"
    t.string   "stripe_id"
    t.string   "stripe_token"
    t.datetime "cycle_ends_on"
    t.boolean  "billing_issue",            default: false
    t.string   "billing_issue_msg",        default: "Unknown, please contact support"
    t.string   "cc_last_four"
    t.string   "plan",                     default: "agent"
    t.integer  "premium_connection_limit", default: 0
    t.index ["deleted_at"], name: "index_accounts_on_deleted_at", using: :btree
  end

  create_table "dashboards", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "name"
    t.integer  "graphs_count"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.text     "positions",    default: [],                 array: true
    t.boolean  "public_form",  default: false
    t.string   "uuid"
    t.datetime "deleted_at"
    t.index ["account_id"], name: "index_dashboards_on_account_id", using: :btree
    t.index ["deleted_at"], name: "index_dashboards_on_deleted_at", using: :btree
  end

  create_table "graphs", force: :cascade do |t|
    t.integer  "dashboard_id"
    t.integer  "integration_id"
    t.string   "kind"
    t.string   "title"
    t.integer  "size",           default: 3
    t.json     "data"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "uuid"
    t.boolean  "public_form",    default: false
    t.datetime "deleted_at"
    t.integer  "error_count",    default: 0
    t.string   "error_msg"
    t.index ["dashboard_id"], name: "index_graphs_on_dashboard_id", using: :btree
    t.index ["deleted_at"], name: "index_graphs_on_deleted_at", using: :btree
    t.index ["integration_id"], name: "index_graphs_on_integration_id", using: :btree
  end

  create_table "integrations", force: :cascade do |t|
    t.integer  "account_id"
    t.json     "auth"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.datetime "deleted_at"
    t.string   "provider"
    t.boolean  "enabled",    default: true
    t.index ["account_id"], name: "index_integrations_on_account_id", using: :btree
    t.index ["deleted_at"], name: "index_integrations_on_deleted_at", using: :btree
  end

  create_table "loops", force: :cascade do |t|
    t.integer  "account_id"
    t.datetime "deleted_at"
    t.integer  "duration",      default: 30
    t.string   "name"
    t.string   "uuid"
    t.string   "transition",    default: "default"
    t.boolean  "public_loop",   default: false
    t.text     "dashboard_ids", default: [],                     array: true
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.index ["account_id"], name: "index_loops_on_account_id", using: :btree
  end

end
