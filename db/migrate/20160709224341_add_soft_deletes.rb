class AddSoftDeletes < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :deleted_at, :datetime
    add_index :accounts, :deleted_at

    add_column :dashboards, :deleted_at, :datetime
    add_index :dashboards, :deleted_at

    add_column :graphs, :deleted_at, :datetime
    add_index :graphs, :deleted_at

    add_column :integrations, :deleted_at, :datetime
    add_index :integrations, :deleted_at
  end
end
