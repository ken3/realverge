class AddGraphPositionsToDashboard < ActiveRecord::Migration[5.0]
  def change
    add_column :dashboards, :positions, :text, array: true, default: []
  end
end
