class PushBackTrialDateForAllAccounts < ActiveRecord::Migration[5.0]
  def change
    Account.update_all(cycle_ends_on: (Time.current + 14.days).to_datetime)
  end
end
