class AddEnableFlagToIntegration < ActiveRecord::Migration[5.0]
  def change
    add_column :integrations, :enabled, :boolean, default: true
  end
end
