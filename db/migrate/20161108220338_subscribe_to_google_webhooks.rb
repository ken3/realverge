class SubscribeToGoogleWebhooks < ActiveRecord::Migration[5.0]
  def change
    Integration.where(provider: 'google_integration').each do |g|
      begin
        g.subscribe_to_webhook
      rescue
      end
    end
  end
end
