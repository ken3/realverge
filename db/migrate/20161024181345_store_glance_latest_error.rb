class StoreGlanceLatestError < ActiveRecord::Migration[5.0]
  def change
    add_column :graphs, :error_msg, :string
  end
end
