class AddNativeToGraphKind < ActiveRecord::Migration[5.0]
  def change
    Graph.all.each do |graph|
      graph.update_columns(kind: "native_#{graph.kind}")
    end
  end
end
