class AddEmailOptInToAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :email_optin, :boolean, default: true
  end
end
