class AddGraphUuid < ActiveRecord::Migration[5.0]
  def change
    add_column :graphs, :uuid, :string, uniq: true
  end
end
