class AddPasswordResetTokenToAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :password_reset_token, :string
    add_column :accounts, :reset_password_date, :datetime
  end
end
