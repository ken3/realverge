class AddTimeZoneToAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :time_zone, :string, default: "Pacific Time (US & Canada)"
  end
end
