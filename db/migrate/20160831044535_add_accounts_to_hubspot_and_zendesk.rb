class AddAccountsToHubspotAndZendesk < ActiveRecord::Migration[5.0]
  def change
    if Rails.env.production?
      Account.all.each do |account|
        account.update_zendesk!
        account.update_hubspot!
      end
    end
  end
end
