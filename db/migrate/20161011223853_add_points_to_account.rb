class AddPointsToAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :engagement_points, :integer
  end
end
