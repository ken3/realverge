class CreateDashboards < ActiveRecord::Migration[5.0]
  def change
    create_table :dashboards do |t|
      t.references :account
      t.string :name
      t.integer :graphs_count

      t.timestamps
    end
  end
end
