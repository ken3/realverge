class AddUuidToDashboard < ActiveRecord::Migration[5.0]
  def change
    add_column :dashboards, :public_form, :boolean, default: false
    add_column :dashboards, :uuid, :string, uniq: true
  end
end
