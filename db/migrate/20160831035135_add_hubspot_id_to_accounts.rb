class AddHubspotIdToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :hubspot_id, :string
  end
end
