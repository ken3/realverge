class AddLastLoginToAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :last_login_at, :datetime, default: :now
  end
end
