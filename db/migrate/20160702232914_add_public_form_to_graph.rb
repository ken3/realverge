class AddPublicFormToGraph < ActiveRecord::Migration[5.0]
  def change
    add_column :graphs, :public_form, :boolean, default: false
  end
end
