class AddReferrerToAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :referrer_id, :integer
  end
end
