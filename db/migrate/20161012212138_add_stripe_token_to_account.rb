class AddStripeTokenToAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :stripe_id,                :string
    add_column :accounts, :stripe_token,             :string
    add_column :accounts, :cycle_ends_on,            :datetime
    add_column :accounts, :billing_issue,            :bool,    default: false
    add_column :accounts, :billing_issue_msg,        :string,  default: 'Unknown, please contact support'
    add_column :accounts, :cc_last_four,             :string
    add_column :accounts, :plan,                     :string,  default: :agent
    add_column :accounts, :premium_connection_limit, :integer, default: 0
  end
end