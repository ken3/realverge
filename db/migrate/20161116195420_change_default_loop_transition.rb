class ChangeDefaultLoopTransition < ActiveRecord::Migration[5.0]
  def change
    change_column_default :loops, :transition, from: 'none', to: 'default'
  end
end
