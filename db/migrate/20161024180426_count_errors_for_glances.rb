class CountErrorsForGlances < ActiveRecord::Migration[5.0]
  def change
    add_column :graphs, :error_count, :integer, default: 0
  end
end
