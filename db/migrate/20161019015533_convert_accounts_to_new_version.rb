class ConvertAccountsToNewVersion < ActiveRecord::Migration[5.0]
  def change
    Account.update_all(cycle_ends_on: (Time.current + 14.days).to_datetime)
    Account.all.each do |account|
      account.create_stripe!
      account.dashboards.delete_all

      dashboard = Dashboard.create(
        account: account,
        name: "Let's Learn 😊"
      )
      dashboard.graphs << Graph.new(size: 3, kind: 'native_number', title: 'This is a Number Glance', data: { output: 'number' })
      IntegrationOperation.generate_zapier!(account)

      account = account.reload
      account.update_zendesk!
      account.update_hubspot!
      sleep 1 # Sleep so the updates are not rate limited.
    end
  end
end