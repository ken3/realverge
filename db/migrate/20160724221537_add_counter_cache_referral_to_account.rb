class AddCounterCacheReferralToAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :referrals_count, :integer, default: 0
  end
end
