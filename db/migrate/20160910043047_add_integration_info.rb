class AddIntegrationInfo < ActiveRecord::Migration[5.0]
  def change
    add_column :integrations, :provider, :string
    rename_column :integrations, :settings, :auth
  end
end
