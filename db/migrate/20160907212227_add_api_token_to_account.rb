class AddApiTokenToAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :api_token, :string
    Account.all.each do |account|
      account.regenerate_api_key!
    end
  end
end
