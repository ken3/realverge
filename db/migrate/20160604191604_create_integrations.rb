class CreateIntegrations < ActiveRecord::Migration[5.0]
  def change
    create_table :integrations do |t|
      t.references :account
      t.json :settings

      t.timestamps
    end
  end
end
