class CreateLoops < ActiveRecord::Migration[5.0]
  def change
    create_table :loops do |t|
      t.references :account
      t.datetime   :deleted_at
      t.integer    :duration,      default: 30
      t.string     :name
      t.string     :uuid,          uniq: true
      t.string     :transition,    default: 'none'
      t.boolean    :public_loop,   default: false
      t.text       :dashboard_ids, array: true, default: []

      t.timestamps
    end
  end
end
