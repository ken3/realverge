class AddLastSeenToAccounts < ActiveRecord::Migration[5.0]
  def change
    add_column :accounts, :last_seen_at, :datetime, default: "now()"
  end
end
