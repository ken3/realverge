class EmailShouldAlwaysBeUniq < ActiveRecord::Migration[5.0]
  def change
    change_column :accounts, :email, :string, unique: true
  end
end
