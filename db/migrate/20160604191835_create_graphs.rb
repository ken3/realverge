class CreateGraphs < ActiveRecord::Migration[5.0]
  def change
    create_table :graphs do |t|
      t.references :dashboard
      t.references :integration
      t.string :kind
      t.string :title
      t.integer :size, default: 3
      
      t.json :data

      t.timestamps
    end
  end
end
