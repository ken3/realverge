account = Account.new({
  email: Faker::Internet.email,
  name: Faker::Name.name,
  phone: Faker::PhoneNumber.phone_number
})

account.password = 'password'

account.save!

dashboard = Dashboard.create({
  account: account,
  name: Faker::App.name
})

integration = Integration.create({
  account: account
})

3.times do
  Graph.create({
    dashboard: dashboard,
    integration: integration,
    size: 3,
    title: Faker::App.name,
    kind: 'number',
    data: { value: rand(9999999) }
  })
end

Graph.create({
  dashboard: dashboard,
  integration: integration,
  size: 3,
  title: Faker::App.name,
  kind: 'image',
  data: { src: Faker::Avatar.image }
})

Graph.create({
  dashboard: dashboard,
  integration: integration,
  size: 3,
  title: Faker::App.name,
  kind: 'line',
  data: { series: (0..50).to_a.shuffle.take(rand(50)), labels: [] }
})

Graph.create({
  dashboard: dashboard,
  integration: integration,
  size: 3,
  title: Faker::App.name,
  kind: 'bar',
  data: { series: (0..50).to_a.shuffle.take(rand(50)), labels: [] }
})

Graph.create({
  dashboard: dashboard,
  integration: integration,
  size: 3,
  title: Faker::App.name,
  kind: 'pie',
  data: { series: (0..3).to_a.shuffle.take(rand(50)), labels: [] }
})

Graph.create({
  dashboard: dashboard,
  integration: integration,
  size: 3,
  title: Faker::App.name,
  kind: 'donut',
  data: { series: (0..3).to_a.shuffle.take(rand(50)), labels: [] }
})

6.times do
  Graph.create({
    dashboard: dashboard,
    integration: integration,
    size: 2,
    title: Faker::App.name,
    kind: 'number',
    data: { value: rand(99999) }
  })
end