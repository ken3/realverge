class IntegrationGenerator < Rails::Generators::Base
  argument :integration_name, type: :string

  def create_form_component
    create_file "app/assets/javascripts/components/glance/form/#{integration_name}.js.jsx", <<-JS
/* global _ React $ ReactDOM #{integration_name.titleize} */
var #{integration_name.titleize} = React.createClass({

  propTypes: {
    glance: React.PropTypes.object,
    updateForm: React.PropTypes.func,
    editing: React.PropTypes.bool
  },

  componentWillMount: function () {
    return true;
  },

  render: function() {
    return <div />
  }
})
    JS
  end

  def create_integration_module
    create_file "app/models/integrations/#{integration_name}_integration.rb", <<-RUBY
module Integrations::#{integration_name.titleize}Integration

  def extract_auth(auth, opts)
    self.auth = auth.credentials
  end

  private

end
    RUBY
  end

  def add_to_providers
    inject_into_file "app/models/providers.yml.erb", before: "# end" do <<-YAML
- :name: #{integration_name.titleize}
  :provider: #{integration_name}_integration
  :desc: ''
  :link: https://www.agentecho.io/connections
  :kind: :premium
      YAML
    end
  end

  def create_git_branch
    git checkout: "-b integrations/#{integration_name}"
  end
end