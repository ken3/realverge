task renormalize_data: :environment do
  Graph.all.each do |graph|
    graph.data = graph.data
    graph.save!
  end
end