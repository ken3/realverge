task nightly_job: :environment do
  EngagementUpdate.perform_async
  GoogleWebhookSubscription.perform_async
  Rake::Task["start_loop"].invoke
end
