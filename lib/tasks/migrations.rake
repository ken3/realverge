namespace :migrations do
  desc "Check if migrations are pending"
  task pending: :environment do
    puts ActiveRecord::Migrator.needs_migration?
  end
end
